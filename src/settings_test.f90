module SettingsTest
use Types, only: dp

use AssertsTest, only: assert_equal, assert_true, assert_approx, &
                       assert_string_starts_with, assert_string_contains

use CommandLineArgs, only: parsed_args, &
                           parse_command_line_arguments_from_str

use Settings, only: read_from_command_line, read_from_parsed_command_line, &
                    program_settings

use CommandLineAddArg, only: add_flag, add_positional_arg, add_arg, &
                             program_args

use String, only: string_starts_with, string_is_empty
implicit none
private

public settings_test_all

contains

! read_from_parsed_command_line
! ----------------

subroutine read_from_parsed_command_line_test__no_args(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_settings) :: settings
    character(len=1024) :: error_message
    logical :: success

    call parse_command_line_arguments_from_str("data.bin", parsed)

    call read_from_parsed_command_line(parsed=parsed, &
                                       settings=settings, &
                                       error_message=error_message, &
                                       success=success)

    call assert_true(success, __FILE__, __LINE__, failures)

    call assert_true(string_is_empty(error_message), &
                     __FILE__, __LINE__, failures)

    call assert_true(.not. settings%help, __FILE__, __LINE__, failures)

    call assert_equal(settings%output_path, 'data.bin', &
                      __FILE__, __LINE__, failures)

    call assert_approx(settings%tmax, 50._dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_equal(settings%nt, 5000, __FILE__, &
                      __LINE__, failures)

    call assert_approx(settings%eccentricity, .7_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_equal(settings%ode_approximator, 'leapfrog', &
                      __FILE__, __LINE__, failures)
end

subroutine read_from_parsed_command_line_test__named(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_settings) :: settings
    character(len=1024) :: error_message
    logical :: success
    character(len=1000) :: args_str

    args_str = "&
&tristana &
&--tmax=3.21 &
&--nt=382 &
&--eccentricity=0.23 &
&--ode_approximator=runge-kutta-4 &
&"

    call parse_command_line_arguments_from_str(args_str, parsed)

    call read_from_parsed_command_line(parsed=parsed, settings=settings, &
                                       error_message=error_message, &
                                       success=success)

    call assert_true(success, __FILE__, __LINE__, failures)

    call assert_true(string_is_empty(error_message), &
                     __FILE__, __LINE__, failures)

    call assert_true(.not. settings%help, __FILE__, __LINE__, failures)

    call assert_equal(settings%output_path, 'tristana', &
                      __FILE__, __LINE__, failures)

    call assert_approx(settings%tmax, 3.21_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_equal(settings%nt, 382, __FILE__, &
                      __LINE__, failures)

    call assert_approx(settings%eccentricity, .23_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)

    call assert_equal(settings%ode_approximator, 'runge-kutta-4', &
                      __FILE__, __LINE__, failures)
end


subroutine show_help_test(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_settings) :: settings
    character(len=2024) :: error_message
    logical :: success
    character(len=1000) :: args_str

    args_str = "data.bin --help"
    call parse_command_line_arguments_from_str(args_str, parsed)

    call read_from_parsed_command_line(parsed=parsed, settings=settings, &
                                       error_message=error_message, &
                                       success=success)

    call assert_true(.not. success, __FILE__, __LINE__, failures)

    call assert_true(.not. string_is_empty(error_message), __FILE__, &
                     __LINE__, failures)

    call assert_true(settings%help, __FILE__, __LINE__, failures)

    call assert_string_starts_with(error_message, &
            NEW_LINE('h')//"Calculate orbit of a body moving around a point", &
            __FILE__, __LINE__, failures)
end


! read_from_parsed_command_line
! --------------

subroutine read_from_command_line_test(failures)
    integer, intent(inout) :: failures
    type(program_settings) :: settings
    logical :: success

    call read_from_command_line(silent=.true., settings=settings, success=success)

    call assert_true(.not. success, __FILE__, __LINE__, failures)
end

subroutine read_from_command_line_unrecognized_test(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_settings) :: settings
    character(len=1024) :: error_message
    logical :: success
    character(len=1000) :: args_str

    args_str = "data.bin --huh?=123"
    call parse_command_line_arguments_from_str(args_str, parsed)

    call read_from_parsed_command_line(parsed=parsed, settings=settings, &
                                       error_message=error_message, &
                                       success=success)

    call assert_true(.not. success, __FILE__, __LINE__, failures)

    call assert_true(.not. settings%help, __FILE__, __LINE__, failures)

    call assert_string_starts_with(error_message, &
        "ERROR: Unrecognized parameter 'huh?'", __FILE__, __LINE__, failures)

    call assert_true(.true., __FILE__, __LINE__, failures)
end


subroutine settings_test_all(failures)
    integer, intent(inout) :: failures

    call read_from_parsed_command_line_test__no_args(failures)
    call read_from_parsed_command_line_test__named(failures)
    call show_help_test(failures)
    call read_from_command_line_test(failures)
    call read_from_command_line_unrecognized_test(failures)
end

end module SettingsTest
