!
! Calculate acceleration of a body from gravity of another body
!
module Acceleration
use Types, only: dp
implicit none
private
public :: get_acceleration

contains

!
! Calculate acceleration of a body due to the force of gravity of
! the point mass
!
! Inputs:
! -------
!
! position : position vector
!
!
! Outputs:
! -------
!
! acceleration : acceleration vector
!
subroutine get_acceleration(position, acceleration)
    real(dp), intent(in)  :: position(3)
    real(dp), intent(out) :: acceleration(3)
    real(dp) :: radius_squared, radius

    radius_squared = dot_product(position, position)
    radius = sqrt(radius_squared)
    acceleration = -position / (radius_squared * radius)
end subroutine

end module Acceleration
