!
! Prints out values of variables for all particles into a text file
!
module Output

use Types, only: dp
use String, only: str, join_strings
use Settings, only: program_settings
use FileUtils, only: create_dir

implicit none
private

public :: write_output


contains


!
! Prints solution to a binary data file. See README.md for description
! of the file format.
!
! Inputs:
! --------
!
! filename : Name of the data file to print output to
!
! times : 1D array containing values of the time coordinate
!
! positions : 2D array containing position of the body for each time value:
!             * First index is space coordinate: 1 for x, 2 for y and 3 for z.
!             * Second index is time, it has same size as `times` array.
!
!             Examples:
!
!             * positions(1, 10) contains value of the x coordinate
!               for 10th time iteration.
!
!             * positions(2, 10) is the value for the y coordinate.
!
!
! velocities : 2D array containing velocities of the body for each time value.
!              Same format as `positions`.
!
! energies : 1D array containing energies of the body for each time value.
!              Same format as `positions`.
!
! angular_momenta : 2D array containing angular momenta of the body
!              for each time value. Same format as `positions`.
!
subroutine write_output(filename, times, positions, velocities, &
                        energies, angular_momenta)
    character(len=*), intent(in) :: filename
    real(dp), intent(in) :: times(:)
    real(dp), intent(in) :: positions(:,:), velocities(:,:)
    real(dp), intent(in) :: energies(:), angular_momenta(:,:)
    integer :: out_unit

    open(newunit=out_unit, file=filename, form="unformatted", action="write", &
        status="replace")

    write(out_unit) size(times)
    write(out_unit) times
    write(out_unit) positions
    write(out_unit) velocities
    write(out_unit) energies
    write(out_unit) angular_momenta

    close(unit=out_unit)
end subroutine

end module Output
