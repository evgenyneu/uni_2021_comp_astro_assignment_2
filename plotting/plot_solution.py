#
# Plot orbits of a body moving around a point mass
#

from plot_utils import create_dir
import matplotlib.pyplot as plt
import numpy as np
import os
from solver import calculate_orbit
import math


def verify_timestep(dt, times):
  """
    Raises exception if the expected time step `dt` is not
    equal to the actual time step returned from the simulation.

    Parameters
    ----------
    dt : float
        Expected time step duration.

    times : list of float
        Array of time variables returned by the simulation.
  """

  dt_actual = times[1] - times[0]

  if (round(dt_actual - dt, 10) > 1e-10):
    raise Exception(f"Actual time step {dt_actual} is not equal "
                    f"to expected {dt}")


def plot(ax, nt, dt, eccentricity, ode_approximator, color):
    """
    Make a plot of the orbit in the x-y plane.

    Parameters
    ----------

    ax : Matplotlib's axis object

    nt : int
        The number of time steps

    dt : float
        Duration of a single time step

    eccentricity : float
        Orbital eccentricity, a number between 0 (circular) to 1.

    ode_approximator : string
        Type of numerical procedure for finding an approximate solution
        of an ordinary differential equation: 'leapfrog', 'runge-kutta-4'.

    color : string
        Color of the plotted trajectory.
    """

    tmax = dt * (nt - 1)

    result = calculate_orbit(tmax=tmax, nt=nt, eccentricity=eccentricity,
                             ode_approximator=ode_approximator)

    (times, positions, velocities, energies, angular_momenta) = result
    verify_timestep(dt=dt, times=times)

    x = positions[:, 0]
    y = positions[:, 1]

    ax.plot(x, y, color=color)

    ax.set_xlabel("Position x [AU]")
    ax.set_ylabel("Position y [AU]")
    ax.grid(color='#dddddd')

    # Plot time step
    # --------

    text = (
        r'$\Delta t=$'
        f'{dt:.2G}'
        r" a$/ 2 \pi$"
    )

    ax.text(0.01, 0.98, text, verticalalignment='top',
      transform=ax.transAxes,
      color='black', fontsize=10)


def plot_trajectories(plot_dir, file_name, colors):
    """
    Compare Leapfrog and Runge-Kutta methods by plotting trajectories of
    the particle for different time steps.

    Parameters
    ----------
    plot_dir : str
        Directory where the plot file will be saved.

    file_name : str
        Name of the plot file

    colors : dict
        Plot colors. Keys are names of the integrators: leapfrog and runge_kutta.
    """

    color_leapfrog = colors['leapfrog']
    color_runge_kutta = colors['runge_kutta']

    fig, axes = plt.subplots(3, 2, figsize=(9,9))

    plot(ax=axes[0, 0],
          nt=5000, dt=0.01, eccentricity=0.7,
          ode_approximator='leapfrog', color=color_leapfrog)

    plot(ax=axes[0, 1],
          nt=5000, dt=0.01, eccentricity=0.7,
          ode_approximator='runge-kutta-4', color=color_runge_kutta)

    plot(ax=axes[1, 0],
          nt=5000, dt=0.05, eccentricity=0.7,
          ode_approximator='leapfrog', color=color_leapfrog)

    plot(ax=axes[1, 1],
          nt=5000, dt=0.05, eccentricity=0.7,
          ode_approximator='runge-kutta-4', color=color_runge_kutta)

    plot(ax=axes[2, 0],
          nt=5000, dt=0.1, eccentricity=0.7,
          ode_approximator='leapfrog', color=color_leapfrog)

    plot(ax=axes[2, 1],
          nt=5000, dt=0.1, eccentricity=0.7,
          ode_approximator='runge-kutta-4', color=color_runge_kutta)

    # Prevent big numbers on x axis labels from overlaping
    axes[2, 1].get_xaxis().get_major_formatter().set_powerlimits((0, 0))

    # Axes limits
    # ------

    axes[0, 0].set_xlim(-2, 0.5)
    axes[0, 1].set_xlim(-2, 0.5)
    axes[0, 0].set_ylim(-0.8, 0.8)
    axes[0, 1].set_ylim(-0.8, 0.8)

    axes[1, 0].set_xlim(-2.5, 1)
    axes[1, 1].set_xlim(-2.5, 1)
    axes[1, 0].set_ylim(-0.8, 1.8)
    axes[1, 1].set_ylim(-0.8, 1.8)

    axes[2, 0].axis('equal')
    axes[2, 1].axis('equal')

    # Titles
    # ------

    axes[0, 0].set_title('Leapfrog', color=color_leapfrog)
    axes[0, 1].set_title('Runge-Kutta 4', color=color_runge_kutta)
    fig.tight_layout()

    # Save plot
    # ---------

    create_dir(plot_dir)
    pdf_file = os.path.join(plot_dir, file_name)
    fig.savefig(pdf_file)


def plot_angular_momentum(ax, nt, dt, eccentricity, ode_approximator, color):
    """
    Make a plot of angular momentum of orbiting body.

    Parameters
    ----------

    ax : Matplotlib's axis object

    nt : int
        The number of time steps

    dt : float
        Duration of a single time step

    eccentricity : float
        Orbital eccentricity, a number between 0 (circular) to 1.

    ode_approximator : string
        Type of numerical procedure for finding an approximate solution
        of an ordinary differential equation: 'leapfrog', 'runge-kutta-4'.

    color : string
        Color of the plotted trajectory.
    """

    tmax = dt * (nt - 1)

    # Exact angular momentum
    # ------
    position = [1 - eccentricity, 0, 0]
    velocity = [0, math.sqrt((1 + eccentricity)/(1 - eccentricity)), 0]
    exact_angular_momentum = np.cross(position, velocity)

    # Calculate orbit
    # ------

    result = calculate_orbit(tmax=tmax, nt=nt, eccentricity=eccentricity,
                             ode_approximator=ode_approximator)

    (times, positions, velocities, energies, angular_momenta) = result
    verify_timestep(dt=dt, times=times)

    x = times
    y = angular_momenta[:, 2] # z-component of angular momenta
    y = y - exact_angular_momentum[2]

    # Plot
    # -------

    ax.plot(x, y, color=color)
    ax.set_xlabel(r"Time t [a$/ 2 \pi$]")
    ax.set_ylabel(r"Absolute error (approx. - exact) [$2 \pi \ AU^2 \ a^{-1}$]")
    ax.grid(color='#dddddd')


def plot_angular_momenta(plot_dir, file_name, colors):
    """
    Compare Leapfrog and Runge-Kutta methods by plotting angular
    momenta vs time.

    Parameters
    ----------
    plot_dir : str
        Directory where the plot file will be saved.

    file_name : str
        Name of the plot file

    colors : dict
        Plot colors. Keys are names of the integrators: leapfrog and runge_kutta.
    """

    color_leapfrog = colors['leapfrog']
    color_runge_kutta = colors['runge_kutta']

    fig, axes = plt.subplots(2, 1, figsize=(8,8))

    plot_angular_momentum(ax=axes[0],
        nt=5000, dt=0.05, eccentricity=0.7,
        ode_approximator='leapfrog', color=color_leapfrog)

    plot_angular_momentum(ax=axes[1],
        nt=5000, dt=0.05, eccentricity=0.7,
        ode_approximator='runge-kutta-4', color=color_runge_kutta)

    # Titles
    # ------

    axes[0].set_title('Leapfrog', color=color_leapfrog)
    axes[1].set_title('Runge-Kutta 4', color=color_runge_kutta)
    fig.tight_layout()

    # Save plot
    # ---------

    create_dir(plot_dir)
    pdf_file = os.path.join(plot_dir, file_name)
    fig.savefig(pdf_file)


def plot_energy(ax, nt, dt, eccentricity, ode_approximator, color):
    """
    Make a plot of energy of the orbiting body.

    Parameters
    ----------

    ax : Matplotlib's axis object

    nt : int
        The number of time steps

    dt : float
        Duration of a single time step

    eccentricity : float
        Orbital eccentricity, a number between 0 (circular) to 1.

    ode_approximator : string
        Type of numerical procedure for finding an approximate solution
        of an ordinary differential equation: 'leapfrog', 'runge-kutta-4'.

    color : string
        Color of the plotted trajectory.
    """

    tmax = dt * (nt - 1)

    # Calculate orbit
    # ------

    result = calculate_orbit(tmax=tmax, nt=nt, eccentricity=eccentricity,
                             ode_approximator=ode_approximator)

    (times, positions, velocities, energies, angular_momenta) = result
    verify_timestep(dt=dt, times=times)

    x = times
    y = energies

    # Plot
    # -------

    ax.plot(x, y, color=color)
    ax.set_xlabel(r"Time t [a$/ 2 \pi$]")
    ax.set_ylabel(r"Energy per solar mass E [$4 \pi \ AU^2 \ a^{-2}$]")
    ax.grid(color='#dddddd')


def plot_energies(plot_dir, file_name, colors):
    """
    Compare Leapfrog and Runge-Kutta methods by plotting energy vs time.

    Parameters
    ----------
    plot_dir : str
        Directory where the plot file will be saved.

    file_name : str
        Name of the plot file

    colors : dict
        Plot colors. Keys are names of the integrators: leapfrog and runge_kutta.
    """

    color_leapfrog = colors['leapfrog']
    color_runge_kutta = colors['runge_kutta']

    fig, axes = plt.subplots(2, 1, figsize=(8,8))

    plot_energy(ax=axes[0],
        nt=5000, dt=0.05, eccentricity=0.7,
        ode_approximator='leapfrog', color=color_leapfrog)

    plot_energy(ax=axes[1],
        nt=5000, dt=0.05, eccentricity=0.7,
        ode_approximator='runge-kutta-4', color=color_runge_kutta)

    # Titles
    # ------

    axes[0].set_title('Leapfrog', color=color_leapfrog)
    axes[1].set_title('Runge-Kutta 4', color=color_runge_kutta)
    fig.tight_layout()

    # Save plot
    # ---------

    create_dir(plot_dir)
    pdf_file = os.path.join(plot_dir, file_name)
    fig.savefig(pdf_file)


def plot_energy_relative_error(ax, nt, dt, eccentricity, ode_approximator, color, exact):
    """
    Make a plot of relative error for energy of the orbiting body.

    Parameters
    ----------

    ax : Matplotlib's axis object

    nt : int
        The number of time steps

    dt : float
        Duration of a single time step

    eccentricity : float
        Orbital eccentricity, a number between 0 (circular) to 1.

    ode_approximator : string
        Type of numerical procedure for finding an approximate solution
        of an ordinary differential equation: 'leapfrog', 'runge-kutta-4'.

    color : string
        Color of the plotted trajectory.
    """

    tmax = dt * (nt - 1)

    # Calculate orbit
    # ------

    result = calculate_orbit(tmax=tmax, nt=nt, eccentricity=eccentricity,
                             ode_approximator=ode_approximator)

    (times, positions, velocities, energies, angular_momenta) = result
    verify_timestep(dt=dt, times=times)

    x = times
    y = abs((np.array(energies) - exact) / exact)

    # Plot
    # -------

    ax.plot(x, y, color=color)
    ax.set_xlabel(r"Time t [a$/ 2 \pi$]")
    ax.set_ylabel(r"Relative error of energy")
    ax.grid(color='#dddddd')


def plot_energies_relative_error(plot_dir, file_name, colors):
    """
    Compare Leapfrog and Runge-Kutta methods by plotting relative error for
    energy vs time.

    Parameters
    ----------
    plot_dir : str
        Directory where the plot file will be saved.

    file_name : str
        Name of the plot file

    colors : dict
        Plot colors. Keys are names of the integrators: leapfrog and runge_kutta.
    """

    color_leapfrog = colors['leapfrog']
    color_runge_kutta = colors['runge_kutta']

    fig, axes = plt.subplots(2, 1, figsize=(8,8))

    plot_energy_relative_error(ax=axes[0],
        nt=5000, dt=0.05, eccentricity=0.7,
        ode_approximator='leapfrog', color=color_leapfrog, exact=-0.5)

    plot_energy_relative_error(ax=axes[1],
        nt=5000, dt=0.05, eccentricity=0.7,
        ode_approximator='runge-kutta-4', color=color_runge_kutta, exact=-0.5)

    # Titles
    # ------

    axes[0].set_title('Leapfrog', color=color_leapfrog)
    axes[1].set_title('Runge-Kutta 4', color=color_runge_kutta)
    fig.tight_layout()

    # Save plot
    # ---------

    create_dir(plot_dir)
    pdf_file = os.path.join(plot_dir, file_name)
    fig.savefig(pdf_file)


def make_plots():
    """
    Create multiple plots.
    """

    plot_dir = "plots"

    colors = {
        "leapfrog": "orangered",
        "runge_kutta": "deeppink"
    }

    plot_trajectories(
        plot_dir=plot_dir,
        file_name="001.01_orbits.pdf",
        colors=colors
    )

    plot_angular_momenta(
        plot_dir=plot_dir,
        file_name="001.02_angular_momenta.pdf",
        colors=colors
    )

    plot_energies(
        plot_dir=plot_dir,
        file_name="001.03_energies.pdf",
        colors=colors
    )

    plot_energies_relative_error(
        plot_dir=plot_dir,
        file_name="001.04_energies_relative_error.pdf",
        colors=colors
    )

    print(f'Plots are saved into "{plot_dir}" directory.')


if __name__ == '__main__':
    make_plots()
    print('We are done!')
