module AccelerationTest
use Types, only: dp
use AssertsTest, only: assert_approx
use Acceleration, only: get_acceleration
implicit none
private
public acceleration_test_all

contains

subroutine get_acceleration_test_one(failures)
    integer, intent(inout) :: failures
    real(dp) :: acceleration(3)
    real(dp) :: position(3) = [1._dp, 0._dp, 0._dp]

    call get_acceleration(position=position, &
                          acceleration=acceleration)

    call assert_approx(acceleration(1), -1._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(acceleration(2), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(acceleration(3), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)
end

subroutine get_acceleration_test_two(failures)
    integer, intent(inout) :: failures
    real(dp) :: acceleration(3)
    real(dp) :: position(3) = [1._dp, 1._dp, 0._dp]

    call get_acceleration(position=position, &
                          acceleration=acceleration)

    call assert_approx(acceleration(1), -0.3535533905932_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(acceleration(2), -0.3535533905932_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(acceleration(3), 0._dp, 1e-10_dp, __FILE__, &
                       __LINE__, failures)
end

subroutine get_acceleration_test_three(failures)
    integer, intent(inout) :: failures
    real(dp) :: acceleration(3)
    real(dp) :: position(3) = [0.3_dp, -0.2_dp, 0.9_dp]

    call get_acceleration(position=position, &
                          acceleration=acceleration)

    call assert_approx(acceleration(1), -0.3291769934868_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(acceleration(2), 0.2194513289912_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(acceleration(3), -0.987530980460_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)
end


subroutine acceleration_test_all(failures)
    integer, intent(inout) :: failures

    call get_acceleration_test_one(failures)
    call get_acceleration_test_two(failures)
    call get_acceleration_test_three(failures)
end

end module AccelerationTest
