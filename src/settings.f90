!
! Program settings supplied by the user used the command line arguments:
!
!   $ ./build/main --output_dir=output --n_particles_initial=100
!
module Settings
use Types, only: dp
use String, only: string_is_empty

use CommandLineArgs, only: parsed_args, &
                           parse_current_command_line_arguments, &
                           ARGUMENT_MAX_LENGTH, &
                           ERROR_MESSAGE_LENGTH

use CommandLineAddArg, only: add_flag, add_positional_arg, add_arg, &
                             program_args

use CommandLineHelp, only: make_help_message

use CommandLineProcessArgs, only: process_parsed_args

implicit none
private

public :: read_from_parsed_command_line, read_from_command_line, allocate_settings

!
! Stores program settings that are supplied by the user
!
type, public :: program_settings
     ! Is .true. if we want to show help message
    logical, pointer :: help

    ! Path to the output file containing solution data
    character(len=ARGUMENT_MAX_LENGTH), pointer :: output_path

    ! The largest t value
    real(dp), pointer :: tmax

    ! The number of time steps
    integer, pointer :: nt

    ! Orbital eccentricity
    real(dp), pointer :: eccentricity

    ! Type of numerical procedure for finding an approximate solution
    ! of an ordinary differential equation: 'leapfrog', 'runge-kutta-4'
    character(len=ARGUMENT_MAX_LENGTH), pointer :: ode_approximator
end type program_settings

! Default values for the settings
! ------

real(dp), parameter :: DEFAULT_TMAX = 5000._dp * 0.01_dp
integer, parameter :: DEFAULT_NT = 5000
real(dp), parameter :: DEFAULT_ECCENTRICITY = 0.7_dp

character(len=*), parameter :: DEFAULT_ODE_APPROXIMATOR = "leapfrog"

character(len=100), parameter :: ALLOWED_ODE_APPROXIMATORS(2) = &
    [character(len=100) :: 'leapfrog', 'runge-kutta-4']


contains


!
! Allocate program settings and set them to default values.
! This is used for setting up settings in unit test because unit
! tests do not parse command line arguments since they are not run from
! command line.
!
! Inputs:
! -------
!
! settings : program settings
!
subroutine allocate_settings(settings)
    type(program_settings), intent(inout) :: settings

    allocate(settings%help)
    settings%help = .false.

    allocate(settings%output_path)
    settings%output_path = "test_output"

    allocate(settings%tmax)
    settings%tmax = DEFAULT_TMAX

    allocate(settings%nt)
    settings%nt = DEFAULT_NT

    allocate(settings%eccentricity)
    settings%eccentricity = DEFAULT_ECCENTRICITY

    allocate(settings%ode_approximator)
    settings%ode_approximator = DEFAULT_ODE_APPROXIMATOR
end subroutine

!
! Reads settings from parsed command line arguments.
! Shows errors to output if command line arguments were incorrect.
!
! Outputs:
! -------
!
! settings : parsed line arguments.
!
! success : .true. if all settings were successfully read and we are ready
!                  to run the program
!
! silent : do not show any output when .true. (used in unit tests)
!
subroutine read_from_command_line(silent, settings, success)
    logical, intent(in) :: silent
    type(program_settings), intent(out) :: settings
    logical, intent(out) :: success
    type(parsed_args) :: parsed
    character(len=ERROR_MESSAGE_LENGTH) :: error_message

    call parse_current_command_line_arguments(parsed)

    call read_from_parsed_command_line(parsed=parsed, settings=settings, &
                                       error_message=error_message, &
                                       success=success)

    if (.not. string_is_empty(error_message)) then
        ! Detected errors in command line arguments
        if (.not. silent) write (0, *) trim(error_message)
        success = .false.
        return
    end if
end subroutine

!
! Reads settings from parsed command line arguments.
!
! Inputs:
! --------
!
! parsed: object containing the parsed command line arguments.
!
!
! Outputs:
! -------
!
! settings : parsed line arguments.
!
! error_message : an error message to be shown to the user. Empty if no error.
!
! success : .true. if there were no errors and we are ready to run the program
!           (i.e. when all required arguments were supplied)
subroutine read_from_parsed_command_line(parsed, settings, error_message, success)
    type(parsed_args), intent(in) :: parsed
    type(program_settings), intent(out) :: settings
    type(program_args), allocatable :: args(:)
    character(len=*), intent(out) :: error_message
    logical, intent(out) :: success
    success = .true.
    error_message = ""

    call add_flag(args, 'help', 'Show this message.', settings%help)

    call add_positional_arg(args, 'OUTPUT', 'Path to the output data file.', &
                            settings%output_path)

    call add_arg(args, 'tmax', &
                 'The largest time value, in dimensionless units.', &
                 DEFAULT_TMAX, settings%tmax)

     call add_arg(args, 'nt', &
                 'Number of time steps.', &
                 DEFAULT_NT, settings%nt)

    call add_arg(args, 'eccentricity', &
                 'Orbital eccentricity, a number from 0 to 1.', &
                 DEFAULT_ECCENTRICITY, settings%eccentricity)

    call add_arg(args, 'ode_approximator', 'Type of numerical procedure for &
&finding an approximate solution of an ordinary differential equation.', &
                 DEFAULT_ODE_APPROXIMATOR, &
                 settings%ode_approximator, &
                 ALLOWED_ODE_APPROXIMATORS)

    call process_parsed_args(parsed, args, error_message, success)

    if (settings%help) then
        error_message = make_help_message( &
            'Calculate orbit of a body moving around a point mass.', &
            './build/main', args, &
            './build/main output.bin --tmax=100 --nt=500 --eccentricity=0.2')

        success = .false. ! We show the help message and not running the program
    end if
end subroutine

end module Settings
