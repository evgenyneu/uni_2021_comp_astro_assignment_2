module OutputTest
use Types, only: dp
use Output, only: write_output
use FileUtils, only: file_exists, delete_dir, delete_file
use AssertsTest, only: assert_true, assert_approx, assert_equal
use Settings, only: program_settings, allocate_settings

implicit none
private
public output_test_all

contains

subroutine write_output_test(failures)
    integer, intent(inout) :: failures
    real(dp) :: times(2), times_read(2)
    real(dp) :: energies(2), energies_read(2)
    real(dp) :: positions(3, 2), velocities(3, 2)
    real(dp) :: positions_read(3, 2), velocities_read(3, 2)
    real(dp) :: angular_momenta(3, 2), angular_momenta_read(3, 2)
    integer :: unit
    integer :: nt

    times = [0._dp, 0.5_dp]
    positions = reshape([1, 2, 3, 4, 5, 6], shape(positions))
    velocities = reshape((/ 11, 12, 13, 14, 15, 16 /), shape(velocities))
    energies = [3._dp, -0.5_dp]
    angular_momenta = reshape([-3, -4, -5, -6, -7, -8], shape(velocities))

    call write_output(filename="test_output.dat", times=times, &
                      positions=positions, velocities=velocities, &
                      energies=energies, angular_momenta=angular_momenta)

    call assert_true(file_exists("test_output.dat"), __FILE__, __LINE__, failures)

    open(newunit=unit, file="test_output.dat", form='unformatted', &
        status='old', action='read' )

    ! nt
    ! --------------

    read (unit) nt
    call assert_equal(nt, 2, __FILE__, __LINE__, failures)


    ! times
    ! --------------

    read (unit) times_read

    call assert_approx(times_read(1), 0._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times_read(2), 0.5_dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)


    ! positions
    ! --------------

    read (unit) positions_read

    call assert_approx(positions_read(1, 1), 1._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions_read(2, 1), 2._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions_read(3, 1), 3._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions_read(1, 2), 4._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions_read(2, 2), 5._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions_read(3, 2), 6._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)


    ! velocities
    ! --------------

    read (unit) velocities_read

    call assert_approx(velocities_read(1, 1), 11._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities_read(2, 1), 12._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities_read(3, 1), 13._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities_read(1, 2), 14._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities_read(2, 2), 15._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities_read(3, 2), 16._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)


    ! energies
    ! --------------

    read (unit) energies_read

    call assert_approx(energies_read(1), 3._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(energies_read(2), -0.5_dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    ! angular_momenta
    ! --------------

    read (unit) angular_momenta_read

    call assert_approx(angular_momenta_read(1, 1), -3._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(angular_momenta_read(2, 1), -4._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(angular_momenta_read(3, 1), -5._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(angular_momenta_read(1, 2), -6._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(angular_momenta_read(2, 2), -7._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(angular_momenta_read(3, 2), -8._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    close(unit=unit)

    call delete_file("test_output.dat")
end


subroutine output_test_all(failures)
    integer, intent(inout) :: failures

    call write_output_test(failures)
end

end module OutputTest
