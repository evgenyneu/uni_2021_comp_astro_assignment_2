module RungeKutta4Test
use Types, only: dp

use AssertsTest, only: assert_equal, assert_true, assert_approx, &
                       assert_string_starts_with, assert_string_contains

use RungeKutta4, only: runge_kutta_4_step

implicit none
private

public runge_kutta_4_test_all

contains

! Used for suppressing 'unused argument' warning
! Source: https://stackoverflow.com/a/37503295
#define unused(x) associate( x => x ); end associate

!
! Calculate derivative for ODE:
!
!   dy/dx = y.
!
! Inputs:
! -------
!
! x, y : values of x and y variables
!
! Outputs:
! -------
!
! dy : derivative dy/dx
!
subroutine dy_exponential(x, y, dy)
    real(dp), intent(in)  :: x, y(:)
    real(dp), intent(out) :: dy(:)

    unused(x)

    ! Derivative is equal to the variable
    dy = y
end subroutine


! Test Runge-Kutta 4 method by solving equation
!
!   dy/dx = y
!
! with initial condition:
!
!   y(x=0) = 1.
!
! Exact analytical solution is
!
!   y(x) = e^x.
!
subroutine step_runge_kutta_4_test__exponential(failures)
    integer, intent(inout) :: failures
    real(dp) :: x, y(1)
    real(dp) :: step_x = 0.1_dp
    real(dp) :: all_x(21), all_y(21, 1)
    integer :: i

    ! Initial conditions
    x = 0.0_dp
    y(1) = 1.0_dp

    ! Iterate from x from 0 to 2
    do i = 1, 21
        all_x(i) = x
        all_y(i, :) = y
        call runge_kutta_4_step(h=step_x, t=x, y=y, f=dy_exponential)
    end do

    ! Verify x
    ! ------------

    call assert_approx(all_x(1), 0.0_dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(all_x(2), 0.1_dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(all_x(11), 1._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(all_x(21), 2._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    ! Verify y
    ! ------------

    ! e^0 = 1
    call assert_approx(all_y(1, 1), 1.0_dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    ! e^0.1 = 1.1051709181
    call assert_approx(all_y(2, 1), 1.1051709181_dp, 1e-7_dp, &
                       __FILE__, __LINE__, failures)

    ! e^1 = 2.7182818285
    call assert_approx(all_y(11, 1), 2.7182818285_dp, 1e-6_dp, &
                       __FILE__, __LINE__, failures)

    ! e^2 = 7.3890560989
    call assert_approx(all_y(21, 1), 7.3890560989_dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)
end


!
! Calculate derivative for ODE:
!
!   dy/dx = 2x.
!
! Inputs:
! -------
!
! x, y : values of x and y variables
!
! Outputs:
! -------
!
! dy : derivative dy/dx
!
subroutine dy_2x(x, y, dy)
    real(dp), intent(in)  :: x, y(:)
    real(dp), intent(out) :: dy(:)

    unused(y)
    dy = 2 * x
end subroutine


! Test Runge-Kutta 4 method by solving equation
!
!   dy/dx = 2x
!
! with initial condition:
!
!   y(x=0) = 0
!
! Exact analytical solution is
!
!   y(x) = x**2
!
subroutine step_runge_kutta_4_test__2x(failures)
    integer, intent(inout) :: failures
    real(dp) :: x, y(1)
    real(dp) :: step_x = 0.1_dp
    real(dp) :: all_x(31), all_y(31, 1)
    integer :: i

    ! Initial conditions
    x = 0._dp
    y(1) = 0._dp

    ! Iterate from x from 0 to 3
    do i = 1, 31
        all_x(i) = x
        all_y(i, :) = y
        call runge_kutta_4_step(h=step_x, t=x, y=y, f=dy_2x)
    end do

    ! Verify x
    ! ------------

    call assert_approx(all_x(1), 0.0_dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(all_x(2), 0.1_dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(all_x(31), 3._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    ! Verify y
    ! ------------

    ! 0^2 = 0
    call assert_approx(all_y(1, 1), 0._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    ! 1^2 = 2
    call assert_approx(all_y(11, 1), 1._dp, 1e-7_dp, &
                       __FILE__, __LINE__, failures)

    ! 2^2 = 4
    call assert_approx(all_y(21, 1), 4._dp, 1e-6_dp, &
                       __FILE__, __LINE__, failures)

    ! 3^2 = 9
    call assert_approx(all_y(31, 1), 9._dp, 1e-5_dp, &
                       __FILE__, __LINE__, failures)
end


!
! Calculate derivative for ODE:
!
!   dx/dt = v
!   dv/dt = -x
!
! Inputs:
! -------
!
! x, y : values of x and y variables
!
! Outputs:
! -------
!
! dy : derivative dy/dx
!
subroutine dy_harmonic_oscillator(t, y, dy)
    real(dp), intent(in)  :: t, y(:)
    real(dp), intent(out) :: dy(:)

    unused(t)
    dy = [y(2), -y(1)]
end subroutine


! Test Runge-Kutta 4 method by solving system of equations of harmonic
! oscillator:
!
!   dx/dt = v
!   dv/dt = -x
!
! where
!
!   x, v are position and velocity,
!   t is time.
!
! with initial conditions:
!
!   x(t=0) = 1
!   v(t=0) = 0
!
! Exact analytical solution is
!
!   x(t) = cos(t)
!   v(t) = -sin(t)
!
subroutine step_runge_kutta_4_test__harmonic_oscillator(failures)
    integer, intent(inout) :: failures
    real(dp) :: t, y(2)
    real(dp) :: step_t
    real(dp) :: all_t(101), all_y(101, 2)
    integer :: i

    step_t = 0.02_dp

    ! Initial conditions
    t = 0._dp
    y = [1._dp, 0._dp]

    ! Iterate from x from 0 to 2
    do i = 1, 101
        all_t(i) = t
        all_y(i, :) = y
        call runge_kutta_4_step(h=step_t, t=t, y=y, f=dy_harmonic_oscillator)
    end do

    ! Verify t
    ! ------------

    call assert_approx(all_t(1), 0.0_dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(all_t(2), 0.02_dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(all_t(101), 2._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    ! Verify position x
    ! ------------

    ! cos(0)
    call assert_approx(all_y(1, 1), 1._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    ! cos(0.02)
    call assert_approx(all_y(2, 1),0.99980000666657776_dp, 1e-12_dp, &
                       __FILE__, __LINE__, failures)

    ! cos(0.04)
    call assert_approx(all_y(3, 1), 0.99920010666097792_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! cos(2)
    call assert_approx(all_y(101, 1), -0.41614683654714241_dp, 1e-8_dp, &
                       __FILE__, __LINE__, failures)

    ! Verify velocity v
    ! ------------

    ! sin(0) = 0
    call assert_approx(all_y(1, 2), 0._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    ! -sin(0.02)
    call assert_approx(all_y(2, 2), -0.01999866669333308_dp, 1e-8_dp, &
                       __FILE__, __LINE__, failures)

    ! -sin(0.04)
    call assert_approx(all_y(3, 2), -0.039989334133315554_dp, 1e-8_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(all_y(101, 2), -0.90929742682568171_dp, 1e-8_dp, &
                       __FILE__, __LINE__, failures)
end


subroutine runge_kutta_4_test_all(failures)
    integer, intent(inout) :: failures

    call step_runge_kutta_4_test__exponential(failures)
    call step_runge_kutta_4_test__2x(failures)
    call step_runge_kutta_4_test__harmonic_oscillator(failures)
end

end module RungeKutta4Test
