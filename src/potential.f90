!
! Working with potential and acceleration
!
module Potential
use Types, only: dp
implicit none
private
public :: get_potential

contains

!
! Calculates gavitational potential.
!
! Inputs:
! -------
!
! position : a position vector
!
! Returns:
! -------
!
! Value of the gravitational potential.
!
real(dp) function get_potential(position)
    real(dp), intent(in) :: position(:)
    real(dp) :: radius, radius_squared

    radius_squared = dot_product(position, position)
    radius = sqrt(radius_squared)
    get_potential = -1 / radius
end function get_potential

end module Potential
