module CommandLineHelpTest
use Types, only: dp

use AssertsTest, only: assert_equal, assert_true, assert_approx, &
                       assert_string_starts_with, assert_string_contains

use CommandLineAddArg, only: program_args, add_arg, add_flag, add_positional_arg
use CommandLineHelp, only: make_help_message
use CommandLineArgs, only: ARGUMENT_MAX_LENGTH, ERROR_MESSAGE_LENGTH

implicit none
private

public command_line_help_test_all

contains

subroutine make_help_message_test(failures)
    integer, intent(inout) :: failures
    type(program_args), allocatable :: args(:)
    character(len=ERROR_MESSAGE_LENGTH) :: result
    character(len=ARGUMENT_MAX_LENGTH), pointer :: output_path
    logical, pointer :: help
    real(dp), pointer :: x_start
    integer, pointer :: nx
    character(len=ARGUMENT_MAX_LENGTH), pointer :: initial_conditions

    call add_positional_arg(args, 'OUTPUT', 'Path to the output data file', &
                            output_path)

    call add_flag(args, 'help', 'Show this message', help)

    call add_arg(args, 'x_start', 'The smallest x value', 0.0_dp, x_start)

    call add_arg(args, 'nx', 'Number of x points in the grid', 100, nx)

    call add_arg(args, 'initial_conditions', 'Type of initial conditions', &
                 'square', &
                 initial_conditions, &
                 [character(len=10) :: 'square', 'sine'])

    result = make_help_message( &
        'This program solves 1D fluid equations.', &
        './build/main', args, &
        './build/main output.bin --nx=100 --initial_conditions=square')

    call assert_string_starts_with(result, &
            NEW_LINE('h')//"This program solves 1D fluid equations", &
            __FILE__, __LINE__, failures)

    call assert_string_contains(result, &
            "    $ ./build/main OUTPUT [OPTIONS]", &
            __FILE__, __LINE__, failures)

    call assert_string_contains(result, &
            "    --x_start: The smallest x value", &
            __FILE__, __LINE__, failures)

    call assert_string_contains(result, &
            "         Default: 100", &
            __FILE__, __LINE__, failures)

    call assert_string_contains(result, &
            "         Values: square, sine", &
            __FILE__, __LINE__, failures)

    call assert_string_contains(result, &
            "    $ ./build/main output.bin --nx=100 --initial_conditions=square", &
            __FILE__, __LINE__, failures)
end

subroutine command_line_help_test_all(failures)
    integer, intent(inout) :: failures

    call make_help_message_test(failures)
end

end module CommandLineHelpTest
