module EnergyTest
use Types, only: dp
use AssertsTest, only: assert_true, assert_equal, assert_approx
use Energy, only: total_energy
implicit none
private
public energy_test_all

contains

subroutine total_energy_test(failures)
    integer, intent(inout) :: failures
    real(dp) :: result
    real(dp) :: position(3) = [1.2_dp, 1.1_dp, 0.1_dp]
    real(dp) :: velocity(3) = [0.21_dp, 1.3_dp, 0.7_dp]

    result = total_energy(position=position, &
                          velocity=velocity)

    call assert_approx(result, 0.49891066051503419_dp, 1e-15_dp, &
                       __FILE__, __LINE__, failures)
end

subroutine energy_test_all(failures)
    integer, intent(inout) :: failures

    call total_energy_test(failures)
end

end module EnergyTest
