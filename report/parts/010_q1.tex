\fancyhead[LE,RO]{Question 1}
\section{Question 1: Leapfrog vs Runge-Kutta 4}

I wrote a Fortran program to calculate trajectory and a Python program to make the plots. The program code is located at

\medskip
{\footnotesize{\url{https://bitbucket.org/evgenyneu/uni_2021_comp_astro_assignment_2}}}
\medskip


\subsection{Trajectories}

Let's look at trajectories for different time steps shown on \autoref{fig_q1_trajectory} and compare results from Leapfrog and Runge-Kutta 4 methods. The choice of units is explained in  \hyperref[sec_units]{Appendix}.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=1\textwidth]{images/001.01_orbits.pdf}
  \caption{Trajectory of a particle moving in gravitational field of a point mass calculated with Leapfrog (left) and Runge-Kutta 4 (right) methods. The duration of the time step $\Delta t$ is shown at the top left corner. Eccentricity is $e=0.7$.}
  \label{fig_q1_trajectory}
\end{figure}

\subsubsection*{Time step $\Delta t = 0.01 \ a/2\pi$}

The trajectories from Leapfrog and Runge-Kutta 4 are almost identical. The line for Leapfrog is a little bit thicker, which means that the orbit changes slightly with time. This is caused by larger errors in numerical approximation of the ODE solution when using the Leapfrog method (``error'' means the difference between the exact and approximation solution). The errors are larger because Leapfrog is a second-order method (errors are proportional to $\Delta t^2$), while Runge-Kutta 4 is a fourth-order method (errors are proportional to $\Delta t^4$).

\subsubsection*{Time step $\Delta t = 0.05 \ a/2\pi$}

The trajectories from Leapfrog start to change significantly with time, while Runge-Kutta method gives smaller changes.


\subsubsection*{Time step $\Delta t = 0.1 \ a/2\pi$}

The trajectories from Leapfrog increase, but Runge-Kutta trajectory is a straight line, I don't know why.



\subsection{Angular momenta}

\sloppy
On \autoref{fig_q1_angular_momenta} I plot the absolute error of the $z$ component of the angular momentum, which is the difference between the exact and approximated angular momenta. The $x$ and $y$ components of the angular momentum remained zero, as expected. We can see that for Leapfrog method, the approximated angular momentum remains constant and agrees with exact solution to almost up to machine precision, which is about $\num{1e-16}$ for the double-precision floating-point values I used the Fortran program. In contrast, for Runge-Kutta 4 method, the angular momentum declines with time and deviates from zero by about $10^{12}$ times the machine precision.

Let's calculate the relative error for Runge-Kutta method. The exact angular momentum is $\SI{0.714142}{\ 2 \pi \ AU^2 \ a^{-1}}$. At the end of the simulation the error was about $\SI{-0.00175} {2 \pi \ AU^2 \ a^{-1}}$. Therefore, the relative error for Runge-Kutta method grew up to
\begin{equation*}
  \left| \frac{-0.00175}{0.714142} \right| \approx 0.0025.
\end{equation*}

Hence, for our simulation, the Leapfrog method conserved angular momentum while Runge-Kutta 4 did not.

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{images/001.02_angular_momenta.pdf}
  \caption{Absolute error of the $z$-component of angular momentum per solar mass for trajectory of an orbiting body calculated with Leapfrog (top) and Runge-Kutta 4 (bottom) methods. The duration of the time step is $\Delta t=0.05 \ a/2\pi$. Eccentricity is $e=0.7$.}
  \label{fig_q1_angular_momenta}
\end{figure}

\pagebreak

\subsection{Energies}

On \autoref{fig_q1_energies} I compare the energies of the orbing body between Leapfrog and Runge-Kutta 4 methods. We can see that in both cases, the energy deviates from exact value of $\SI{-0.5}{4 \pi \ AU^2 \ a^{-2}}$. To estimate this deviation, we plot the relative errors on \autoref{fig_q1_energies_relative_error}. The approximated energy deviates from the exact value by about $7\%$ and $3\%$ for the Leapfrog and Runge-Kutta 4 methods respectively. Hence, neither of the methods conserve the energy in our simulation.

However, the methods differ in how energy error changes with time. For Leapfrog, the relative error changes periodically from $0\%$ to $7\%$ for the entire duration of the simulation. We can see that the error depends on the position of the orbiting body and the error approaches zero when the body is near its staring position. Hence, the Leapfrog method did conserve energy in our simulation if we consider the same positions of the body, to which it returns after each orbiting period. In contrast, for Runge-Kutta 4 method, the relative error increased steadily with time.

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{images/001.03_energies.pdf}
  \caption{Energy of an orbiting body calculated with Leapfrog (top) and Runge-Kutta 4 (bottom) methods. The duration of the time step is $\Delta t=0.05 \ a/2\pi$. Eccentricity is $e=0.7$.}
  \label{fig_q1_energies}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{images/001.04_energies_relative_error.pdf}
  \caption{Relative error of energy of an orbiting body calculated with Leapfrog (top) and Runge-Kutta 4 (bottom) methods. The duration of the time step is $\Delta t=0.05 \ a/2\pi$. Eccentricity is $e=0.7$.}
  \label{fig_q1_energies_relative_error}
\end{figure}


