! Leapfrog integrator for calculating
! orbit of a body
module Leapfrog
use Types, only: dp
implicit none
private
public :: leapfrog_step

interface
    !
    ! Interface of the function that calculates acceleration
    !
    ! Inputs:
    ! -------
    !
    ! position : a position vector
    !
    !
    ! Outputs:
    ! -------
    !
    ! acceleration : acceleration vector
    !
    subroutine acceleration_interface(position, acceleration)
        use types, only: dp
        real(dp), intent(in)  :: position(3)
        real(dp), intent(out) :: acceleration(3)
    end subroutine
end interface

contains

!
! Use one step of Leapfrog method to calculate
! updated position, velocity and acceleration of an
! orbiting body.
!
!
! Inputs:
! -------
!
! dt : Time step
!
! acceleration_fn : function for calculating acceleration
!
!
! Outputs:
! -------
!
! position : Position vector
!
! velocity : Velocity vector
!
! acceleration : Acceleration vector
!
subroutine leapfrog_step(dt, acceleration_fn, position, velocity, &
                              acceleration)

    real(dp), intent(in)    :: dt
    real(dp), intent(inout) :: position(3), velocity(3), acceleration(3)
    procedure(acceleration_interface) :: acceleration_fn

    velocity = velocity + 0.5_dp * dt * acceleration
    position = position + dt * velocity
    call acceleration_fn(position=position, acceleration=acceleration)
    velocity = velocity + 0.5_dp * dt * acceleration
end subroutine

end module Leapfrog
