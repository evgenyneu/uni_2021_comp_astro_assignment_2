module LeapfrogTest
use Types, only: dp
use AssertsTest, only: assert_approx
use Leapfrog, only: leapfrog_step
use Acceleration, only: get_acceleration

implicit none
private
public leapfrog_test_all

contains

subroutine step_leapfrog_test_zero_dt(failures)
    integer, intent(inout) :: failures
    real(dp) :: position(3) = [1, 0, 0]
    real(dp) :: velocity(3) = [0, 1, 0]
    real(dp) :: acceleration(3)

    call get_acceleration(position=position, acceleration=acceleration)

    call leapfrog_step(dt=0._dp, acceleration_fn=get_acceleration,&
                       position=position, velocity=velocity, &
                       acceleration=acceleration)

    call assert_approx(position(1), 1._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(position(2), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(position(3), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocity(1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocity(2), 1._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocity(3), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(acceleration(1), -1._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(acceleration(2), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(acceleration(3), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)
end

subroutine step_leapfrog_test(failures)
    integer, intent(inout) :: failures
    real(dp) :: position(3) = [1, 0, 0]
    real(dp) :: velocity(3) = [0, 1, 1]
    real(dp) :: acceleration(3)

    call get_acceleration(position=position, acceleration=acceleration)

    call leapfrog_step(dt=0.01_dp, acceleration_fn=get_acceleration, &
                       position=position, velocity=velocity, &
                       acceleration=acceleration)

    call assert_approx(position(1), 0.99995_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(position(2), 0.01_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(position(3), 0.01_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocity(1), -0.9999000112E-002_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocity(2), 0.99995000749_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocity(3), 0.9999500074_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(acceleration(1), -0.99980002249_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(acceleration(2), -0.99985001499e-2_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(acceleration(3), -0.999850014998e-2_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)
end


subroutine leapfrog_test_all(failures)
    integer, intent(inout) :: failures

    call step_leapfrog_test_zero_dt(failures)
    call step_leapfrog_test(failures)
end

end module LeapfrogTest
