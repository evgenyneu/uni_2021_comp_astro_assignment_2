! The entry point to the program
program Main
use Orbit, only: read_settings_calculate_orbit_and_create_output
implicit none

call read_settings_calculate_orbit_and_create_output(silent=.false.)

end program Main
