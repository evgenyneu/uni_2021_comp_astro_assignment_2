!
! Calculates approximate solution for a system of differential
! equations usign Runge-Kutta fourth order method.
!
module RungeKutta4
use Types, only: dp

implicit none
private

public :: runge_kutta_4_step

interface
    subroutine derivative_interface(t, y, dy)
        use types, only: dp
        real(dp), intent(in)  :: t, y(:)
        real(dp), intent(out) :: dy(:)
    end subroutine
end interface

contains

!
! Does one iteration of Runge-Kutta 4 method for a system of differential
! equations, for example:
!
!     dx/dt = f(t,x,y,z)
!     dy/dt = f(t,x,y,z)
!     dz/dt = f(t,x,y,z)
!
! where
!
!   t is an independent variable,
!   x, y and z are dependent variables,
!   f is a known function.
!
! The goal of this subroutine is to approximate values of x, y and z at
! new value of t + h, given the values of x, y, z at t. Here h is a small
! number called 'step size'.
!
! Inputs:
! -------
!
! h : The size of the step for the independent variable.
!     For the example above, this will be the size of the t step.
!
! f : Function that calculates derivatives of all variables y with respect to t.
!
!       Parameters of function f
!       ------------------------
!
!       Inputs:
!       -------
!
!       t : value of independent variable.
!
!       y : Array of values of dependent variables, [x, y, z]
!           for the example above.
!
!       Outputs:
!       -------
!
!       dy : Returns dedivatives of the dependent variables with respect to
!            t, [dx/dt, dy/dt, dz/dt] for the example above.
!
!
! Outputs:
! -------
!
! t : Value of the independent variable with
!     respect to which we integrate (i.e. `t` in example above). The
!     value is updated to t + h.
!
! y : Array of values of dependent variables, [x, y, z] for the example above.
!     The subroutine updates these values with new ones corresponding to
!     incremented independent variable t + h.
!
subroutine runge_kutta_4_step(h, t, y, f)
    real(dp), intent(in) :: h
    real(dp), intent(inout) :: t, y(:)
    procedure(derivative_interface) :: f
    real(dp) :: k1(size(y)), k2(size(y)), k3(size(y)), k4(size(y))

    call f(t = t,             y = y,                dy=k1)
    call f(t = t + h / 2,     y = y + h * k1 / 2,   dy=k2)
    call f(t = t + h / 2,     y = y + h * k2 / 2,   dy=k3)
    call f(t = t + h,         y = y + h * k3,       dy=k4)

    t = t + h

    ! Update dependent variables using the weighted derivatives
    y = y + 1._dp / 6._dp * h * (k1 + 2 * k2 + 2 * k3 + k4)
end

end module RungeKutta4
