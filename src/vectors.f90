!
! Working with vectors
!
module Vectors
use Types, only: dp
implicit none
private
public :: cross_product

contains

!
! Calcualtes a cross produce between two vectors.
!
! Inputs:
! -------
!
! a, b : vectors.
!
! Outputs:
! -------
!
! result : cross product of `a` and `b`.
!
subroutine cross_product(a, b, result)
    real(dp), intent(in)  :: a(:), b(:)
    real(dp), intent(out) :: result(:)

    result(1) = a(2) * b(3) - a(3) * b(2)
    result(2) = a(3) * b(1) - a(1) * b(3)
    result(3) = a(1) * b(2) - a(2) * b(1)
end subroutine

end module Vectors