program MainTest
    use CommandLineAddArgTest, only: command_line_add_arg_test_all
    use CommandLineHelpTest, only: command_line_help_test_all
    use CommandLineProcessArgsTest, only: command_line_process_args_test_all
    use CommandLineArgsTest, only: command_line_args_test_all
    use StringTest, only: string_test_all
    use FloatUtilsTest, only: float_utils_test_all
    use FileUtilsTest, only: file_utils_test_all
    use SettingsTest, only: settings_test_all
    use OutputTest, only: output_test_all
    use AccelerationTest, only: acceleration_test_all
    use LeapfrogTest, only: leapfrog_test_all
    use OrbitTest, only: orbit_test_all
    use RungeKutta4Test, only: runge_kutta_4_test_all
    use PotentialTest, only: potential_test_all
    use EnergyTest, only: energy_test_all
    use VectorsTest, only: vectors_test_all
    implicit none

    integer :: failures = 0
    character(len=1024) :: test_word = "TESTS"

    ! Tests for the libraries
    call command_line_args_test_all(failures)
    call command_line_add_arg_test_all(failures)
    call command_line_help_test_all(failures)
    call command_line_process_args_test_all(failures)
    call string_test_all(failures)
    call float_utils_test_all(failures)
    call file_utils_test_all(failures)
    call vectors_test_all(failures)

    ! Program tests
    call settings_test_all(failures)
    call output_test_all(failures)
    call acceleration_test_all(failures)
    call leapfrog_test_all(failures)
    call orbit_test_all(failures)
    call runge_kutta_4_test_all(failures)
    call potential_test_all(failures)
    call energy_test_all(failures)

    if (failures == 0) then
        print *, NEW_LINE('h')//'Tests finished successfully'
    else
        if (failures == 1) then
            test_word = "TEST"
        end if

        print '(a, i4, 1x, a, 1x, a)', NEW_LINE('h'), failures, &
              trim(test_word), 'FAILED'

        call exit(42)
    end if
end program MainTest
