! Calculate orbit of a body moving around a point mass
module Orbit
use Types, only: dp
use Settings, only: program_settings, read_from_command_line
use FloatUtils, only: linspace
use Acceleration, only: get_acceleration
use OdeSolver, only: solve_step
use Output, only: write_output
use Energy, only: total_energy
use Vectors, only: cross_product

implicit none
private
public :: calculate_orbit, init_data, initial_conditions, &
          calculate_orbit_and_create_output, &
          read_settings_calculate_orbit_and_create_output

contains

!
! Initialize arrays for storing values for time, positions and velocities.
!
!
! Inputs:
! -------
!
! nt : number of time values
!
!
! Outputs:
! -------
!
! times : 1D array containing values of the time coordinate
!
! positions : 2D array containing position of the body for each time value:
!             * First index is space coordinate: 1 for x, 2 for y and 3 for z.
!             * Second index is time, it has same size as `times` array.
!
!             Examples:
!
!             * positions(1, 10) contains value of the x coordinate
!               for 10th time iteration.
!
!             * positions(2, 10) is the value for the y coordinate.
!
!
! velocities : 2D array containing velocities of the body for each time value.
!              Same format as `positions`.
!
subroutine init_data(nt, times, positions, velocities, &
                     energies, angular_momenta)

    integer, intent(in) :: nt
    real(dp), allocatable, intent(out) :: times(:)
    real(dp), allocatable, intent(out) :: positions(:,:), velocities(:,:)
    real(dp), allocatable, intent(out) :: energies(:), angular_momenta(:,:)
    integer :: stat
    character(len=*), parameter :: error_message = "Error allocating memory"

    ! Allocate times array
    ! ----------

    allocate(times(nt), stat=stat)

    if (stat /= 0) then
        write (0, *) error_message
        call exit(41)
    end if


    ! Allocate positions array
    ! ----------

    allocate(positions(3, nt))

    if (stat /= 0) then
        write (0, *) error_message
        call exit(41)
    end if


    ! Allocate velocities array
    ! ----------

    allocate(velocities(3, nt))

    if (stat /= 0) then
        write (0, *) error_message
        call exit(41)
    end if

    ! Allocate energies array
    ! ----------

    allocate(energies(nt), stat=stat)

    if (stat /= 0) then
        write (0, *) error_message
        call exit(41)
    end if


    ! Allocate angular momenta array
    ! ----------

    allocate(angular_momenta(3, nt))

    if (stat /= 0) then
        write (0, *) error_message
        call exit(41)
    end if

    ! Assign zero values to the arrays
    ! ----------

    times = 0
    positions = 0
    velocities = 0
    energies = 0
    angular_momenta = 0
end subroutine


!
! Calculate initial values for position, velocity and acceleration
! of the orbiting body
!
!
! Inputs:
! -------
!
! eccentricity : orbital eccentricity
!
!
! Outputs:
! -------
!
! position : position vector
!
! velocity : velocity vector
!
! acceleration : acceleration vector
!
subroutine initial_conditions(eccentricity, position, &
                              velocity, acceleration)

    real(dp), intent(in) :: eccentricity
    real(dp), intent(out) :: position(3), velocity(3), acceleration(3)

    position = [1.0_dp - eccentricity, 0._dp, 0._dp]

    velocity = 0
    velocity(2) = sqrt( (1 + eccentricity) / (1 - eccentricity) )

    call get_acceleration(position=position, &
                          acceleration=acceleration)
end subroutine


!
! Calculate the orbit of a body moving around a point mass
!
!
! Inputs:
! -------
!
! options : program options
!
!
! Outputs:
! -------
!
! times : 1D array containing values of the time coordinate
!
! positions : 2D array containing position of the body for each time value:
!             * First index is space coordinate: 1 for x, 2 for y and 3 for z.
!             * Second index is time, it has same size as `times` array.
!
!             Examples:
!
!             * positions(1, 10) contains value of the x coordinate
!               for 10th time iteration.
!
!             * positions(2, 10) is the value for the y coordinate.
!
!
! velocities : 2D array containing velocities of the body for each time value.
!              Same format as `positions`.
!
! energies : 1D array containing energies of the body for each time value.
!              Same format as `positions`.
!
! angular_momenta : 2D array containing angular momenta of the body
!              for each time value. Same format as `positions`.
!
subroutine calculate_orbit(options, times, positions, velocities, &
                           energies, angular_momenta)

    type(program_settings), intent(in) :: options
    real(dp), allocatable, intent(out) :: times(:)
    real(dp), allocatable, intent(out) :: positions(:,:), velocities(:,:)
    real(dp), allocatable, intent(out) :: energies(:), angular_momenta(:,:)
    integer :: nt, it
    real(dp) :: tmax, eccentricity, dt
    real(dp) :: position(3), velocity(3), acceleration(3)

    ! Assign shortcut variables from settings
    ! ----------

    nt = options%nt
    tmax = options%tmax
    eccentricity = options%eccentricity

    call init_data(nt=nt, times=times, positions=positions, &
                   velocities=velocities, energies=energies, &
                   angular_momenta=angular_momenta)

    ! Assign evenly spaced time values between 0 and tmax, inclusive
    call linspace(from=0._dp, to=tmax, array=times)

    ! Calculate the time step
    dt = times(2) - times(1)


    ! Set initial conditions
    ! -----------

    call initial_conditions(eccentricity=eccentricity, position=position, &
                            velocity=velocity, acceleration=acceleration)

    ! Calculate the orbit
    do it = 1, nt
        if (it > 1) then
            call solve_step(options=options, dt=dt, t=times(it), &
                            position=position, &
                            velocity=velocity, acceleration=acceleration)
        end if


        positions(:, it) = position
        velocities(:, it) = velocity
        energies(it) = total_energy(position=position, velocity=velocity)

        call cross_product(a=position, b=velocity, &
                           result=angular_momenta(:, it))
    end do
end subroutine

!
! Calculates orbit and prints solutions to a file
!
! Inputs:
! -------
!
! options : program options
!
subroutine calculate_orbit_and_create_output(options)
    type(program_settings), intent(in) :: options
    real(dp), allocatable :: times(:)
    real(dp), allocatable :: positions(:,:), velocities(:,:)
    real(dp), allocatable :: energies(:), angular_momenta(:,:)

    call calculate_orbit(options=options, times=times, &
                         positions=positions, velocities=velocities, &
                         energies=energies, angular_momenta=angular_momenta)

    call write_output(filename=options%output_path, times=times, &
                      positions=positions, velocities=velocities, &
                      energies=energies, angular_momenta=angular_momenta)
end subroutine


!
! Read program settings from command line arguments,
! calculate the orbit and print solutions to a file
!
! Inputs:
! -------
!
! silent : do not show any output if .true. (used in unit tests)
!
subroutine read_settings_calculate_orbit_and_create_output(silent)
    logical, intent(in) :: silent
    type(program_settings) :: settings
    logical :: success

    call read_from_command_line(silent=silent, settings=settings, &
                                success=success)

    if (.not. success) then
        if (.not. silent) call exit(41)
        return
    end if

    call calculate_orbit_and_create_output(options=settings)

    if (.not. silent) then
        print "(a, a, a)", "Solution saved to '", &
            trim(settings%output_path), "'"
    end if
end subroutine


end module Orbit
