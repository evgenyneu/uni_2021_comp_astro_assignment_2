module PotentialTest
use Types, only: dp
use AssertsTest, only: assert_true, assert_equal, assert_approx
use Potential, only: get_potential
use Settings, only: program_settings
implicit none
private
public potential_test_all

contains

! get_potential
! -----------------

subroutine get_potential_test(failures)
    integer, intent(inout) :: failures
    real(dp) :: position(3) = [1.1_dp, 0.9_dp, 12.64_dp]
    real(dp) :: result

    result = get_potential(position=position)

    call assert_approx(result, -0.07861849020794144_dp, 1e-15_dp, &
                       __FILE__, __LINE__, failures)
end


subroutine potential_test_all(failures)
    integer, intent(inout) :: failures

    call get_potential_test(failures)
end


end module PotentialTest
