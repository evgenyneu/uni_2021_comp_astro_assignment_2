module OrbitTest
use Types, only: dp
use Constants, only: pi
use AssertsTest, only: assert_equal, assert_true, assert_approx
use FileUtils, only: file_exists, delete_file
use Settings, only: program_settings, allocate_settings

use Orbit, only: calculate_orbit, init_data, initial_conditions, &
                 calculate_orbit_and_create_output, &
                 read_settings_calculate_orbit_and_create_output

implicit none
private
public orbit_test_all

contains

subroutine init_data_test(failures)
    integer, intent(inout) :: failures
    real(dp), allocatable :: positions(:,:), velocities(:,:)
    real(dp), allocatable :: energies(:), angular_momenta(:,:)
    real(dp), allocatable :: times(:)

    call init_data(nt=99, times=times, positions=positions, &
                   velocities=velocities, energies=energies, &
                   angular_momenta=angular_momenta)


    ! times
    ! ----------

    call assert_equal(size(times), 99, __FILE__, __LINE__, failures)

    ! Ensure all values are zero
    call assert_true(all(abs(times) < 1e-14), __FILE__, __LINE__, failures)

    ! positions
    ! ----------

    call assert_equal(size(positions, 1), 3, __FILE__, __LINE__, failures)
    call assert_equal(size(positions, 2), 99, __FILE__, __LINE__, failures)

    ! Ensure all values are zero
    call assert_true(all(abs(positions) < 1e-14), __FILE__, __LINE__, failures)

    ! velocities
    ! ----------

    call assert_equal(size(velocities, 1), 3, __FILE__, __LINE__, failures)
    call assert_equal(size(velocities, 2), 99, __FILE__, __LINE__, failures)

    ! Ensure all values are zero
    call assert_true(all(abs(velocities) < 1e-14), __FILE__, __LINE__, failures)

    ! energies
    ! ----------

    call assert_equal(size(energies), 99, __FILE__, __LINE__, failures)

    ! Ensure all values are zero
    call assert_true(all(abs(energies) < 1e-14), __FILE__, &
                      __LINE__, failures)

    ! angular momenta
    ! ----------

    call assert_equal(size(angular_momenta, 1), 3, __FILE__, &
                      __LINE__, failures)

    call assert_equal(size(angular_momenta, 2), 99, __FILE__, &
                      __LINE__, failures)

    ! Ensure all values are zero
    call assert_true(all(abs(angular_momenta) < 1e-14), &
                      __FILE__, __LINE__, failures)
end


subroutine initial_conditions_test(failures)
    integer, intent(inout) :: failures
    real(dp) :: position(3), velocity(3), acceleration(3)

    call initial_conditions(eccentricity=0.3_dp, position=position, &
                            velocity=velocity, acceleration=acceleration)


    ! position
    ! ----------

    call assert_approx(position(1), 0.7_dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(position(2), 0._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(position(3), 0._dp, 1e-13_dp, &
                       __FILE__, __LINE__, failures)


    ! velocity
    ! ----------

    call assert_approx(velocity(1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocity(2), 1.362770287738_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocity(3), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)


    ! acceleration
    ! ----------

    call assert_approx(acceleration(1), -2.0408163265306_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(acceleration(2), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(acceleration(3), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)
end


subroutine calculate_orbit_test__leapfrog(failures)
    integer, intent(inout) :: failures
    type(program_settings) :: options
    real(dp), allocatable :: positions(:,:), velocities(:,:)
    real(dp), allocatable :: energies(:), angular_momenta(:,:)
    real(dp), allocatable :: times(:)

    call allocate_settings(options)
    options%tmax = 10 * pi
    options%nt = 3142
    options%eccentricity = 0
    options%ode_approximator = 'leapfrog'

    call calculate_orbit(options=options, times=times, &
                         positions=positions, velocities=velocities, &
                         energies=energies, angular_momenta=angular_momenta)


    ! times
    ! ----------

    call assert_equal(size(times), 3142, __FILE__, __LINE__, failures)

    call assert_approx(times(1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(2), 0.01000188683_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(3), 0.02000377366_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(3141), 31.4059246491_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(3142), 31.4159265359_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)


    ! positions
    ! ----------

    call assert_equal(size(positions, 1), 3, __FILE__, __LINE__, failures)
    call assert_equal(size(positions, 2), 3142, __FILE__, __LINE__, failures)

    ! Initial position

    call assert_approx(positions(1, 1), 1._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! After one iteration

    call assert_approx(positions(1, 2), 0.9999499811_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 2), 0.01000188683091_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 2), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle pi/2

    call assert_approx(positions(1, 158), 0.00050245082754839_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 158), 1.0000248703756_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 158), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle pi

    call assert_approx(positions(1, 315), -1.00004940722_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 315), 0.0011049130080_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 315), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle 10 * pi, returned to initial state

    call assert_approx(positions(1, 3142), 0.9999994513_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 3142), -0.00104755262807_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 3142), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)


    ! velocities
    ! ----------

    call assert_equal(size(velocities, 1), 3, __FILE__, __LINE__, failures)
    call assert_equal(size(velocities, 2), 3142, __FILE__, __LINE__, failures)

    ! Initial position

    call assert_approx(velocities(1, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 1), 1._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! After one iteration

    call assert_approx(velocities(1, 2), -0.010001636670_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 2), 0.99994998113_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 2), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle pi/2

    call assert_approx(velocities(1, 158), -0.999974865239_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 158), 0.000527435138589_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 158), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle pi

    call assert_approx(velocities(1, 315), -0.00110482815387_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 315), -0.999949374542_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 315), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle 10 * pi, returned to initial state

    call assert_approx(velocities(1, 3142), 0.0010475460769_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 3142), 0.9999994513226_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 3142), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! Ensure energies are constant
    ! ---------

    call assert_true(all(abs(energies + 0.5) < 1e-6), __FILE__, &
                     __LINE__, failures)

    ! Angular momentum
    ! ---------

    call assert_true(all(abs(angular_momenta(1, :)) < 1e-6), __FILE__, &
                     __LINE__, failures)

    call assert_true(all(abs(angular_momenta(2, :)) < 1e-6), __FILE__, &
                     __LINE__, failures)

    call assert_true(all(abs(angular_momenta(3, :) - 1) < 1e-13), __FILE__, &
                     __LINE__, failures)
end


subroutine calculate_orbit_test__eccentric__leapfrog(failures)
    integer, intent(inout) :: failures
    type(program_settings) :: options
    real(dp), allocatable :: times(:)
    real(dp), allocatable :: positions(:,:), velocities(:,:)
    real(dp), allocatable :: energies(:), angular_momenta(:,:)

    call allocate_settings(options)
    options%tmax = 10 * pi
    options%nt = 314160
    options%eccentricity = 0.3_dp
    options%ode_approximator = 'leapfrog'

    call calculate_orbit(options=options, times=times, &
                         positions=positions, velocities=velocities, &
                         energies=energies, angular_momenta=angular_momenta)


    ! times
    ! ----------

    call assert_equal(size(times), 314160, __FILE__, __LINE__, failures)

    call assert_approx(times(1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(2), 0.100000084466e-3_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(3), 0.20000016893e-3_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(314159), 31.4158265358_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(314160), 31.4159265359_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)


    ! positions
    ! ----------

    call assert_equal(size(positions, 1), 3, __FILE__, __LINE__, failures)
    call assert_equal(size(positions, 2), 314160, __FILE__, __LINE__, failures)

    ! Initial position

    call assert_approx(positions(1, 1), 0.7_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! After one iteration

    call assert_approx(positions(1, 2), 0.69999998979590_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 2), 0.13627714388e-3_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 2), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! After 15708 iterations

    call assert_approx(positions(1, 15708), -0.5836367878_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 15708), 0.9147626335_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 15708), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)


    ! At angle 10 * pi, returned to initial state

    call assert_approx(positions(1, 314160), 0.7_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 314160), -0.41067122640e-6_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 314160), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)


    ! velocities
    ! ----------

    call assert_equal(size(velocities, 1), 3, __FILE__, __LINE__, failures)
    call assert_equal(size(velocities, 2), 314160, __FILE__, __LINE__, failures)

    ! Initial position

    call assert_approx(velocities(1, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 1), 1.3627702877384_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! After one iteration

    call assert_approx(velocities(1, 2), -0.204081802207e-3_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 2), 1.362770267873_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 2), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! After 15708 iterations

    call assert_approx(velocities(1, 15708), -0.88373394802_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 15708), -0.24935441153_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 15708), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle 10 * pi, returned to initial state

    call assert_approx(velocities(1, 314160), 0.64851489604e-6_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 314160), 1.362770287738_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 314160), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! Ensure energies are constant
    ! ---------

    call assert_true(all(abs(energies + 0.5) < 1e-6), __FILE__, &
                     __LINE__, failures)

    ! Angular momentum
    ! ---------

    call assert_true(all(abs(angular_momenta(1, :)) < 1e-6), __FILE__, &
                     __LINE__, failures)

    call assert_true(all(abs(angular_momenta(2, :)) < 1e-6), __FILE__, &
                     __LINE__, failures)

    call assert_true( &
        all(abs(angular_momenta(3, :) - 0.95393920141694555) < 1e-7), &
        __FILE__, __LINE__, failures)
end


subroutine calculate_orbit_and_create_output_test(failures)
    integer, intent(inout) :: failures
    type(program_settings) :: options
    integer :: unit
    integer :: nt
    real(dp) :: times(3142)
    real(dp), allocatable :: positions(:,:), velocities(:,:)

    call allocate_settings(options)
    options%output_path = "test_output2.dat"
    options%tmax = 10 * pi
    options%nt = 3142
    options%eccentricity = 0
    options%ode_approximator = 'leapfrog'

    allocate(positions(3, 3142))
    allocate(velocities(3, 3142))

    call calculate_orbit_and_create_output(options=options)

    call assert_true(file_exists("test_output2.dat"), __FILE__, &
                                 __LINE__, failures)

    open(newunit=unit, file="test_output2.dat", form='unformatted', &
         status='old', action='read' )

    ! nt
    ! ----------

    read (unit) nt
    call assert_equal(nt, 3142, __FILE__, __LINE__, failures)

    ! times
    ! --------------

    read (unit) times

    call assert_equal(size(times), 3142, __FILE__, __LINE__, failures)

    call assert_approx(times(1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(2), 0.01000188683_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(3), 0.02000377366_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(3141), 31.4059246491_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(3142), 31.4159265359_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)


    ! positions
    ! --------------

    read (unit) positions

    call assert_equal(size(positions, 1), 3, __FILE__, __LINE__, failures)
    call assert_equal(size(positions, 2), 3142, __FILE__, __LINE__, failures)

    ! Initial position

    call assert_approx(positions(1, 1), 1._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! After one iteration

    call assert_approx(positions(1, 2), 0.9999499811_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 2), 0.01000188683091_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 2), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle pi/2

    call assert_approx(positions(1, 158), 0.00050245082754839_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 158), 1.0000248703756_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 158), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle pi

    call assert_approx(positions(1, 315), -1.00004940722_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 315), 0.0011049130080_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 315), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle 10 * pi, returned to initial state

    call assert_approx(positions(1, 3142), 0.9999994513_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 3142), -0.00104755262807_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 3142), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)


    ! velocities
    ! ----------

    read (unit) velocities

    call assert_equal(size(velocities, 1), 3, __FILE__, __LINE__, failures)
    call assert_equal(size(velocities, 2), 3142, __FILE__, __LINE__, failures)

    ! Initial position

    call assert_approx(velocities(1, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 1), 1._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! After one iteration

    call assert_approx(velocities(1, 2), -0.010001636670_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 2), 0.99994998113_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 2), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle pi/2

    call assert_approx(velocities(1, 158), -0.999974865239_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 158), 0.000527435138589_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 158), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle pi

    call assert_approx(velocities(1, 315), -0.00110482815387_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 315), -0.999949374542_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 315), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle 10 * pi, returned to initial state

    call assert_approx(velocities(1, 3142), 0.0010475460769_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 3142), 0.9999994513226_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 3142), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    close(unit=unit)

    call delete_file("test_output2.dat")
end


subroutine calculate_orbit_test__runge_kutta(failures)
    integer, intent(inout) :: failures
    type(program_settings) :: options
    real(dp), allocatable :: positions(:,:), velocities(:,:)
    real(dp), allocatable :: energies(:), angular_momenta(:,:)
    real(dp), allocatable :: times(:)

    call allocate_settings(options)
    options%tmax = 10 * pi
    options%nt = 3142
    options%eccentricity = 0
    options%ode_approximator = 'runge-kutta-4'

    call calculate_orbit(options=options, times=times, &
                         positions=positions, velocities=velocities, &
                         energies=energies, angular_momenta=angular_momenta)


    ! times
    ! ----------

    call assert_equal(size(times), 3142, __FILE__, __LINE__, failures)

    call assert_approx(times(1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(2), 0.01000188683_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(3), 0.02000377366_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(3141), 31.4059246491_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(times(3142), 31.4159265359_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)


    ! positions
    ! ----------

    call assert_equal(size(positions, 1), 3, __FILE__, __LINE__, failures)
    call assert_equal(size(positions, 2), 3142, __FILE__, __LINE__, failures)

    ! Initial position

    call assert_approx(positions(1, 1), 1._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! After one iteration

    call assert_approx(positions(1, 2), 0.9999499811_dp, 1e-8_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 2), 0.01000188683091_dp, 1e-4_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 2), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle pi/2

    call assert_approx(positions(1, 158), 0.00050009427226835_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 158), 0.99999987479428698_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 158), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle pi

    call assert_approx(positions(1, 315), -0.999999499490_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 315), 0.001000187783890_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 315), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle 10 * pi, returned to initial state

    call assert_approx(positions(1, 3142), 0.99999999991_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(2, 3142), 0.9262680357099E-8_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(positions(3, 3142), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)


    ! velocities
    ! ----------

    call assert_equal(size(velocities, 1), 3, __FILE__, __LINE__, failures)
    call assert_equal(size(velocities, 2), 3142, __FILE__, __LINE__, failures)

    ! Initial position

    call assert_approx(velocities(1, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 1), 1._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 1), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! After one iteration

    call assert_approx(velocities(1, 2), -0.01000172007145_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 2), 0.999949981546884_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 2), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle pi/2

    call assert_approx(velocities(1, 158), -0.9999998751093_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 158), 0.00050009411397_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 158), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle pi

    call assert_approx(velocities(1, 315), -0.00100018778020338_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 315), -0.99999950012916439_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 315), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    ! At angle 10 * pi, returned to initial state

    call assert_approx(velocities(1, 3142), -0.9262684451047E-8_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(2, 3142), 1.00000000004367_dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(velocities(3, 3142), 0._dp, 1e-10_dp, &
                       __FILE__, __LINE__, failures)



    ! Ensure energies are constant
    ! ---------

    call assert_true(all(abs(energies + 0.5) < 1e-6), __FILE__, &
                     __LINE__, failures)

    ! Angular momentum
    ! ---------


    call assert_true(all(abs(angular_momenta(1, :)) < 1e-6), __FILE__, &
                     __LINE__, failures)

    call assert_true(all(abs(angular_momenta(2, :)) < 1e-6), __FILE__, &
                     __LINE__, failures)

    call assert_true( &
        all(abs(angular_momenta(3, :) - 1) < 1e-9), &
        __FILE__, __LINE__, failures)
end


subroutine read_settings_calculate_orbit_and_create_output_test(failures)
    integer, intent(inout) :: failures

    call read_settings_calculate_orbit_and_create_output(silent=.true.)

    call assert_true(.true., __FILE__, __LINE__, failures)
end


subroutine orbit_test_all(failures)
    integer, intent(inout) :: failures

    call init_data_test(failures)
    call initial_conditions_test(failures)
    call calculate_orbit_test__leapfrog(failures)
    call calculate_orbit_test__eccentric__leapfrog(failures)
    call calculate_orbit_test__runge_kutta(failures)
    call calculate_orbit_and_create_output_test(failures)
    call read_settings_calculate_orbit_and_create_output_test(failures)
end

end module OrbitTest
