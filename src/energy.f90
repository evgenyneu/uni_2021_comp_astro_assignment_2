!
! Calculate energy of orbiting body
!
module Energy
use Types, only: dp
use Potential, only: get_potential
implicit none
private
public :: total_energy

contains

!
! Calculate total energy of orbiting body
!
! Inputs:
! -------
!
! position : position vector
!
! velocity : position vector
!
!
! Returns:
! -------
!
! Total energy
!
real(dp) function total_energy(position, velocity)
    real(dp), intent(in)  :: position(3), velocity(3)

    total_energy = 0.5_dp * dot_product(velocity, velocity) + &
                        get_potential(position=position)
end function

end module Energy
