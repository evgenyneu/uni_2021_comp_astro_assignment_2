!
! Calculates new positions and velocity using either Leapfrog or
! Runge-Kutta 4 methods.
!
module OdeSolver
use Types, only: dp
use Leapfrog, only: leapfrog_step
use RungeKutta4, only: runge_kutta_4_step
use Acceleration, only: get_acceleration
use Settings, only: program_settings


implicit none
private

public :: solve_step

contains

! Used for suppressing 'unused argument' warning
! Source: https://stackoverflow.com/a/37503295
#define unused(x) associate( x => x ); end associate


!
! Calculate derivative position position and velocity that are needed by
! Runge-Kutta 4 ODE solver.
!
! Inputs:
! -------
!
! t : value of time variable
!
! y : first three elements are x,y,z coordinates of position,
!      and last thre are coordinate of velocity.
!
! Outputs:
! -------
!
! dy : first three elements are x,y,z coordinates of velocity,
!      and last thre are coordinate of acceleration.
!
subroutine derivatives(t, y, dy)
    real(dp), intent(in)  :: t, y(:)
    real(dp), intent(out) :: dy(:)
    real(dp) :: acceleration(3)
    unused(t)

    call get_acceleration(position=y(1:3), acceleration=acceleration)
    dy(1:3) = y(4:6)
    dy(4:6) = acceleration
end subroutine


!
! Calculates new position and velocity of an orbiting body using either
! Leapfrog or Runge-Kutta 4 methods.
!
!
! Inputs:
! -------
!
! options : program options
!
! dt : time step
!
! t : value of the time variable
!
!
! Outputs:
! -------
!
! position : Position vector
!
! velocity : Velocity vector
!
! acceleration : Acceleration vector
!
subroutine solve_step(options, dt, t, position, velocity, acceleration)
    type(program_settings), intent(in) :: options
    real(dp), intent(in)    :: dt, t
    real(dp), intent(inout) :: position(3), velocity(3), acceleration(3)
    real(dp) :: y(6), t_use

    y(1:3) = position
    y(4:6) = velocity

    select case (trim(options%ode_approximator))
    case ('leapfrog')
        call leapfrog_step(dt=dt, acceleration_fn=get_acceleration,&
                       position=position, velocity=velocity, &
                       acceleration=acceleration)

    case ('runge-kutta-4')
        t_use = t
        call runge_kutta_4_step(h=dt, t=t_use, y=y, f=derivatives)
        position = y(1:3)
        velocity = y(4:6)

    case default
        write (0, *) "Unexpected ODE approximator '" // &
            trim(options%ode_approximator) // "'"

        call exit(321)

    end select
end

end module OdeSolver
