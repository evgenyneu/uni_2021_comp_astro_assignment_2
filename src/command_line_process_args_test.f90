module CommandLineProcessArgsTest
use Types, only: dp

use AssertsTest, only: assert_equal, assert_true, assert_approx, &
                       assert_string_starts_with, assert_string_contains

use CommandLineArgs, only: parsed_args, parse_command_line_arguments_from_str

use CommandLineAddArg, only: program_args, add_arg, add_flag, add_positional_arg

use CommandLineProcessArgs, only: process_parsed_args

use CommandLineArgs, only: ARGUMENT_MAX_LENGTH, ERROR_MESSAGE_LENGTH

use String, only: string_is_empty

implicit none
private

public command_line_process_args_test_all

contains

subroutine process_parsed_args_test(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_args), allocatable :: args(:)
    character(len=ERROR_MESSAGE_LENGTH) :: error_message
    logical :: success
    character(len=ARGUMENT_MAX_LENGTH), pointer :: pos_value
    real(dp), pointer :: real_value
    integer, pointer :: int_value
    character(len=ARGUMENT_MAX_LENGTH), pointer :: str_value
    logical, pointer :: flag_value
    character(len=1000) :: args_str

    args_str = "pos_val --real=12.3 --int=12 --str=hello --flag "

    call parse_command_line_arguments_from_str(args_str, parsed)

    call add_positional_arg(args, 'pos', 'Descr pos', pos_value)
    call add_arg(args, 'real', 'Descr real', 12.2_dp, real_value)
    call add_arg(args, 'int', 'Descr int', 32, int_value)

    call add_arg(args, 'str', 'Descr str', 'hi', str_value, &
                 [character(len=10) :: 'hello', 'b'])

    call add_flag(args, 'flag', 'Descr flag', flag_value)


    call process_parsed_args(parsed, args, error_message, success)


    call assert_true(success, __FILE__, __LINE__, failures)

    call assert_true(string_is_empty(error_message), &
                     __FILE__, __LINE__, failures)

    call assert_equal(pos_value, "pos_val", __FILE__, __LINE__, failures)

    call assert_approx(real_value, 12.3_dp, 1e-5_dp, __FILE__, &
                       __LINE__, failures)

    call assert_equal(int_value, 12, __FILE__, __LINE__, failures)

    call assert_equal(str_value, "hello", __FILE__, __LINE__, failures)

    call assert_true(flag_value, __FILE__, __LINE__, failures)
end


! named integer
! --------------

subroutine process_parsed_args_test__named_integer(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_args), allocatable :: args(:)
    character(len=ERROR_MESSAGE_LENGTH) :: error_message
    logical :: success
    integer, pointer :: int_value
    character(len=1000) :: args_str

    args_str = "--int=123"

    call parse_command_line_arguments_from_str(args_str, parsed)

    call add_arg(args, 'int', 'Descr int', 3, int_value)


    call process_parsed_args(parsed, args, error_message, success)


    call assert_true(success, __FILE__, __LINE__, failures)

    call assert_true(string_is_empty(error_message), &
                     __FILE__, __LINE__, failures)

    call assert_equal(int_value, 123, __FILE__, __LINE__, failures)
end

subroutine process_parsed_args_test__named_integer_default(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_args), allocatable :: args(:)
    character(len=ERROR_MESSAGE_LENGTH) :: error_message
    logical :: success
    integer, pointer :: int_value
    character(len=1000) :: args_str

    args_str = ""

    call parse_command_line_arguments_from_str(args_str, parsed)

    call add_arg(args, 'int', 'Descr int', 3, int_value)


    call process_parsed_args(parsed, args, error_message, success)


    call assert_true(success, __FILE__, __LINE__, failures)

    call assert_true(string_is_empty(error_message), &
                     __FILE__, __LINE__, failures)

    call assert_equal(int_value, 3, __FILE__, __LINE__, failures)
end

subroutine process_parsed_args_test__named_integer_nan(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_args), allocatable :: args(:)
    character(len=ERROR_MESSAGE_LENGTH) :: error_message
    logical :: success
    integer, pointer :: int_value
    character(len=1000) :: args_str

    args_str = "--myarg=wow"

    call parse_command_line_arguments_from_str(args_str, parsed)

    call add_arg(args, 'myarg', 'Descr int', 3, int_value)


    call process_parsed_args(parsed, args, error_message, success)


    call assert_true(.not. success, __FILE__, __LINE__, failures)

    call assert_string_starts_with(error_message, &
        "ERROR: 'myarg' is not a number.", &
        __FILE__, __LINE__, failures)

end


! named real
! --------------

subroutine process_parsed_args_test__named_real(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_args), allocatable :: args(:)
    character(len=ERROR_MESSAGE_LENGTH) :: error_message
    logical :: success
    real(dp), pointer :: real_value
    real(dp), pointer :: real_value2
    character(len=1000) :: args_str

    args_str = "--rea=12.3 --another=1.2e-4"

    call parse_command_line_arguments_from_str(args_str, parsed)

    call add_arg(args, 'rea', 'Descr real', 3.1_dp, real_value)
    call add_arg(args, 'another', 'Descr real2', 1.1_dp, real_value2)


    call process_parsed_args(parsed, args, error_message, success)


    call assert_true(success, __FILE__, __LINE__, failures)

    call assert_true(string_is_empty(error_message), &
                     __FILE__, __LINE__, failures)

    call assert_approx(real_value, 12.3_dp, 1e-5_dp, __FILE__, __LINE__, failures)
    call assert_approx(real_value2, .00012_dp, 1e-6_dp, __FILE__, __LINE__, failures)
end

subroutine process_parsed_args_test__named_real_default(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_args), allocatable :: args(:)
    character(len=ERROR_MESSAGE_LENGTH) :: error_message
    logical :: success
    real(dp), pointer :: real_value
    character(len=1000) :: args_str

    args_str = ""

    call parse_command_line_arguments_from_str(args_str, parsed)

    call add_arg(args, 'int', 'Descr real', 3.12_dp, real_value)


    call process_parsed_args(parsed, args, error_message, success)


    call assert_true(success, __FILE__, __LINE__, failures)

    call assert_true(string_is_empty(error_message), &
                     __FILE__, __LINE__, failures)

    call assert_approx(real_value, 3.12_dp, 1e-5_dp, __FILE__, __LINE__, failures)
end

subroutine process_parsed_args_test__named_real__not_a_number(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_args), allocatable :: args(:)
    character(len=ERROR_MESSAGE_LENGTH) :: error_message
    logical :: success
    real(dp), pointer :: real_value
    character(len=1000) :: args_str

    args_str = "--mywarg=wow"

    call parse_command_line_arguments_from_str(args_str, parsed)

    call add_arg(args, 'mywarg', 'Descr real', 3.0_dp, real_value)

    call process_parsed_args(parsed, args, error_message, success)


    call assert_true(.not. success, __FILE__, __LINE__, failures)

    call assert_string_starts_with(error_message, &
        "ERROR: 'mywarg' is not a number.", &
        __FILE__, __LINE__, failures)

end


! named string
! -------------

subroutine process_parsed_args_test__named_string(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_args), allocatable :: args(:)
    character(len=ERROR_MESSAGE_LENGTH) :: error_message
    logical :: success
    character(len=ARGUMENT_MAX_LENGTH), pointer :: str_value
    character(len=1000) :: args_str

    args_str = "--str=hello"

    call parse_command_line_arguments_from_str(args_str, parsed)

    call add_arg(args, 'str', 'Descr str', 'hi', str_value)


    call process_parsed_args(parsed, args, error_message, success)


    call assert_true(success, __FILE__, __LINE__, failures)

    call assert_true(string_is_empty(error_message), &
                     __FILE__, __LINE__, failures)

    call assert_equal(str_value, "hello", __FILE__, __LINE__, failures)
end

subroutine process_parsed_args_test__named_string_not_allowed(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_args), allocatable :: args(:)
    character(len=ERROR_MESSAGE_LENGTH) :: error_message
    logical :: success
    character(len=ARGUMENT_MAX_LENGTH), pointer :: str_value
    character(len=1000) :: args_str

    args_str = "--str=bad"

    call parse_command_line_arguments_from_str(args_str, parsed)

    call add_arg(args, 'str', 'Descr str', 'hi', str_value, &
                 [character(len=10) :: 'hello', 'b'])


    call process_parsed_args(parsed, args, error_message, success)


    call assert_true(.not. success, __FILE__, __LINE__, failures)

    call assert_string_starts_with(error_message, &
        "ERROR: Incorrect value of 'str' argument. &
&Allowed values are: hello, b.", &
        __FILE__, __LINE__, failures)

    call assert_equal(str_value, "bad", __FILE__, __LINE__, failures)
end

subroutine process_parsed_args_test__named_string_default(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_args), allocatable :: args(:)
    character(len=ERROR_MESSAGE_LENGTH) :: error_message
    logical :: success
    character(len=ARGUMENT_MAX_LENGTH), pointer :: str_value
    character(len=100) :: args_str

    args_str = ""
    call parse_command_line_arguments_from_str(args_str, parsed)


    call add_arg(args, 'str', 'Descr str', 'hi', str_value)


    call process_parsed_args(parsed, args, error_message, success)


    call assert_true(success, __FILE__, __LINE__, failures)

    call assert_true(string_is_empty(error_message), &
                     __FILE__, __LINE__, failures)

    call assert_equal(str_value, "hi", __FILE__, __LINE__, failures)
end


! positional
! -------------

subroutine process_parsed_args_test__positional(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_args), allocatable :: args(:)
    character(len=ERROR_MESSAGE_LENGTH) :: error_message
    logical :: success
    character(len=ARGUMENT_MAX_LENGTH), pointer :: pos_value
    character(len=1000) :: args_str

    args_str = "hi_there"

    call parse_command_line_arguments_from_str(args_str, parsed)

    call add_positional_arg(args, 'pos', 'Descr pos', pos_value)


    call process_parsed_args(parsed, args, error_message, success)


    call assert_true(success, __FILE__, __LINE__, failures)

    call assert_true(string_is_empty(error_message), &
                     __FILE__, __LINE__, failures)

    call assert_equal(pos_value, "hi_there", __FILE__, __LINE__, failures)
end

subroutine process_parsed_args_test__positional_missing(failures)
    integer, intent(inout) :: failures
    type(parsed_args) :: parsed
    type(program_args), allocatable :: args(:)
    character(len=ERROR_MESSAGE_LENGTH) :: error_message
    logical :: success
    character(len=ARGUMENT_MAX_LENGTH), pointer :: pos_value

    call parse_command_line_arguments_from_str("", parsed)

    call add_positional_arg(args, 'pos', 'Descr pos', pos_value)


    call process_parsed_args(parsed, args, error_message, success)


    call assert_true(.not. success, __FILE__, __LINE__, failures)

    call assert_string_starts_with(error_message, &
        "ERROR: 'pos' parameter is missing.", &
        __FILE__, __LINE__, failures)
end


subroutine command_line_process_args_test_all(failures)
    integer, intent(inout) :: failures

    call process_parsed_args_test(failures)

    call process_parsed_args_test__named_integer(failures)
    call process_parsed_args_test__named_integer_default(failures)
    call process_parsed_args_test__named_integer_nan(failures)


    call process_parsed_args_test__named_real(failures)
    call process_parsed_args_test__named_real_default(failures)
    call process_parsed_args_test__named_real__not_a_number(failures)

    call process_parsed_args_test__named_string(failures)
    call process_parsed_args_test__named_string_default(failures)
    call process_parsed_args_test__named_string_not_allowed(failures)

    call process_parsed_args_test__positional(failures)
    call process_parsed_args_test__positional_missing(failures)
end

end module CommandLineProcessArgsTest
