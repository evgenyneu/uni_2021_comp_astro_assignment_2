!
! Read parsed command line arguments and store their values
!
module CommandLineProcessArgs
use Types, only: dp

use CommandLineArgs, only: ARGUMENT_MAX_LENGTH, parsed_args, &
                           unrecognized_named_args, has_flag, &
                           get_named_value_or_default, get_positional_value

use CommandLineAddArg, only: program_args

use String, only: join_strings

implicit none
private

public :: process_parsed_args

contains

!
! Make an error message shown to the user
!
! Inputs:
! --------
!
! text: message to be shown
!
!
! Outputs:
! -------
!
! message : the full message shown to user.
!
subroutine make_message(text, message)
    character(len=*), intent(in) :: text
    character(len=*), intent(out) :: message

     write(message, '(a, a, a, a, a)') "ERROR: ", trim(text), ".", NEW_LINE('h'), &
        "Run with --help for help."
end subroutine

subroutine check_unrecognized_params(parsed, args, error_message, success)
    type(parsed_args), intent(in) :: parsed
    type(program_args), allocatable, intent(in) :: args(:)
    character(len=*), intent(out) :: error_message
    logical, intent(out) :: success
    integer :: i, positional
    character(len=ARGUMENT_MAX_LENGTH), allocatable :: valid_args(:)
    character(len=ARGUMENT_MAX_LENGTH), allocatable :: unrecognized(:)
    integer :: unrecognized_count
    success = .true.
    positional = 0

    allocate(valid_args(size(args)))

    do i = 1, size(args)
        valid_args(i) = args(i)%name
    end do

    call unrecognized_named_args(valid=valid_args, parsed=parsed, &
        unrecognized=unrecognized, count=unrecognized_count)

    if (unrecognized_count > 0) then
        call make_message( &
            "Unrecognized parameter '"//trim(unrecognized(1))//"'", &
            error_message)

        success = .false.
    end if
end subroutine


!
! Make an error message shown to the user
!
! Inputs:
! --------
!
! parsed : parsed arguments.
!
! args : array of all arguments used in this program
!
!
! Outputs:
! -------
!
! error_message : error message. Empty if no error.
!
! success : .true. if arguments were processed correctly and there were
!           no errors.
!
subroutine process_parsed_args(parsed, args, error_message, success)
    type(parsed_args), intent(in) :: parsed
    type(program_args), allocatable, intent(in) :: args(:)
    character(len=*), intent(out) :: error_message
    character(len=len(error_message)) :: msg
    logical, intent(out) :: success
    type(program_args) :: arg
    integer :: i, positional
    success = .true.
    positional = 0
    error_message = ""

    do i = 1, size(args)
        arg = args(i)

        select case (arg%data_type)
        case ('logical')
            arg%parsed_value_logical = has_flag(name=arg%name, parsed=parsed)

        case ('integer')
            call get_named_value_or_default( &
                name=arg%name, parsed=parsed, &
                default=arg%default_value_integer, &
                value=arg%parsed_value_integer, success=success)

            if (.not. success) then
                call make_message("'"//arg%name//"' is not a number", error_message)
                return
            end if

        case ('real_dp')
            call get_named_value_or_default( &
                name=arg%name, parsed=parsed, &
                default=arg%default_value_real_dp, &
                value=arg%parsed_value_real_dp, success=success)

            if (.not. success) then
                call make_message("'"//arg%name//"' is not a number", error_message)
                return
            end if

        case ('string')
            if (arg%type == 'positional') then
                positional = positional + 1

                call get_positional_value(index=positional, parsed=parsed, &
                            value=arg%parsed_value_string, &
                            success=success)

                if (.not. success) then
                    call make_message("'"//arg%name//"' parameter is missing", error_message)
                    return
                end if
            else
                call get_named_value_or_default( &
                    name=arg%name, parsed=parsed, &
                    default=arg%default_value_string, &
                    value=arg%parsed_value_string, success=success)

                if (size(arg%allowed_values) > 0 .and. &
                    (.not. any(arg%allowed_values == arg%parsed_value_string))) &
                    then

                    call join_strings(arg%allowed_values, ", ", msg)
                    msg = "Incorrect value of '"//trim(arg%name)//"' argument. &
&Allowed values are: "//trim(msg)

                    call make_message(msg, error_message)
                    success = .false.
                    return
                end if
            end if

        case default
            call make_message("Incorrect data_type", error_message)
            success = .false.
            return

        end select
    end do

    call check_unrecognized_params(parsed, args, error_message, success)
    if (.not. success) return
end subroutine

end module CommandLineProcessArgs
