from solver import read_solution_from_file, calculate_orbit
from pytest import approx
import numpy as np


def test_read_solution_from_file():
    (times, positions, velocities, energies, angular_momenta) = \
      read_solution_from_file("plotting/test_data/test_output.dat")

    # times
    # -------

    times = times.tolist()
    assert len(times) == 2
    assert times == [0, 0.5]

    print(type(positions))

    # positions
    # -------

    assert positions.shape == (2, 3)
    assert positions[0, :].tolist() == [1, 2, 3]
    assert positions[1, :].tolist() == [4, 5, 6]

    # velocities
    # -------

    assert velocities.shape == (2, 3)
    assert velocities[0, :].tolist() == [11, 12, 13]
    assert velocities[1, :].tolist() == [14, 15, 16]

    # energies
    # -------

    energies = energies.tolist()
    assert len(energies) == 2
    assert energies == [3, -0.5]

    # angular momenta
    # -------

    assert angular_momenta.shape == (2, 3)
    assert angular_momenta[0, :].tolist() == [-3, -4, -5]
    assert angular_momenta[1, :].tolist() == [-6, -7, -8]


def test_calculate_orbit():
    result = calculate_orbit(tmax=10 * np.pi, nt=3142, eccentricity=0,
                             ode_approximator='leapfrog')

    (times, positions, velocities, energies, angular_momenta) = result

    # times
    # -------

    times = times.tolist()
    assert len(times) == 3142
    assert times[0] == 0
    assert times[1] == approx(0.01000188683, rel=1e-10)
    assert times[3141] == approx(31.4159265359, rel=1e-10)

    # positions
    # -------

    assert positions.shape == (3142, 3)

    # Initial position
    assert positions[0, :] == approx([1, 0, 0], rel=1e-10)

    # After one iteration
    assert positions[1, :] == approx(
        [0.9999499811, 0.01000188683091, 0],
        rel=1e-10)

    # At angle pi/2
    assert positions[157, :] == approx(
        [0.0005024508275483, 1.0000248703756, 0],
        rel=1e-10)

    # At angle 10 * pi, returned to initial state
    assert positions[3141, :] == approx(
        [0.9999994513, -0.00104755262807, 0],
        rel=1e-10)

    # velocities
    # -------

    assert velocities.shape == (3142, 3)

    # Initial position
    assert velocities[0, :] == approx([0, 1, 0], rel=1e-10)

    # After one iteration
    assert velocities[1, :] == approx(
        [-0.010001636670, 0.99994998113, 0],
        rel=1e-10)

    # At angle pi/2
    assert velocities[157, :] == approx(
        [-0.999974865239, 0.000527435138589, 0],
        rel=1e-10)

    # At angle 10 * pi, returned to initial state
    assert velocities[3141, :] == approx(
        [0.0010475460769, 0.9999994513226, 0],
        rel=1e-10)

    # energies
    # -------

    energies = energies.tolist()
    assert len(energies) == 3142
    assert energies[0] == -0.5
    assert energies[1] == approx(-0.5, rel=1e-10)
    assert energies[3141] == approx(-0.5, rel=1e-10)

    # angular momenta
    # -------

    assert angular_momenta.shape == (3142, 3)

    # Initial position
    assert angular_momenta[0, :] == approx([0, 0, 1], rel=1e-10)

    # After one iteration
    assert angular_momenta[1, :] == approx([0, 0, 1], rel=1e-10)

    # At angle pi/2
    assert angular_momenta[157, :] == approx([0, 0, 1], rel=1e-10)

    # At angle 10 * pi, returned to initial state
    assert angular_momenta[3141, :] == approx([0, 0, 1], rel=1e-10)
