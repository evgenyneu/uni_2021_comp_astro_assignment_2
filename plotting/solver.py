# Calculate orbit for a body
import subprocess
from plot_utils import create_dir
import numpy as np
import os
import struct
import array


def read_solution_from_file(path_to_data):
    """
    Read solution from a binary file. Please refer to README.md
    for description of the binary file format used here.

    Parameters
    ----------
    path_to_data : str
        Path to the binary file containing solution data.


    Returns
    -------
        (times, positions, velocities, energies, angular_momenta) tuple

        times: list of float
            The time values.

        positions: numpy.ndarray
            2D array of positions. First index is time index. Second index is
            the space dimension: 0 for x, 1 for y, 2 for z.

            Example: positions[10, 2] is z position for the 10th time index.

        velocities: numpy.ndarray
            2D array of velocities, same format as positions.

        energies: list of float
            The energy values.

        angular_momenta: numpy.ndarray
            2D array of angular momenta, same format as positions.
    """

    data = open(path_to_data, "rb").read()

    # nt: number of t points
    # ----------

    start = 0
    end = 4*3
    (_, nt, _) = struct.unpack("@iii", data[start: end])

    # times
    # ---------

    start = end + 4
    end = start + nt * 8
    times = array.array("d")
    times.frombytes(data[start:end])

    # Positions: 2D array
    # ---------

    start = end + 8
    end = start + nt * 3 * 8
    positions = array.array("d")
    positions.frombytes(data[start:end])
    positions = np.reshape(positions, (nt, 3), order='C')

    # Velocities: 2D array
    # ---------

    start = end + 8
    end = start + nt * 3 * 8
    velocities = array.array("d")
    velocities.frombytes(data[start:end])
    velocities = np.reshape(velocities, (nt, 3), order='C')

    # Energies
    # ---------

    start = end + 8
    end = start + nt * 8
    energies = array.array("d")
    energies.frombytes(data[start:end])

    # Angular momenta
    # ---------

    start = end + 8
    end = start + nt * 3 * 8
    angular_momenta = array.array("d")
    angular_momenta.frombytes(data[start:end])
    angular_momenta = np.reshape(angular_momenta, (nt, 3), order='C')

    return (times, positions, velocities, energies, angular_momenta)


def calculate_orbit(tmax, nt, eccentricity, ode_approximator):
    """
    Runs Fortran program calculates orbit for a body moving
    around a point mass.

    Parameters
    ----------
    tmax : float
        The largest time value

    nt : int
        The number of time steps

    eccentricity : float
        Orbital eccentricity, a number between 0 (circular) to 1.

    ode_approximator : string
        Type of numerical procedure for finding an approximate solution
        of an ordinary differential equation: 'leapfrog', 'runge-kutta-4'.


    Returns
    -------
        (times, positions, velocities, energies, angular_momenta) tuple

        times: list of float
            The time values.

        positions: numpy.ndarray
            2D array of positions. First index is time index. Second index is
            the space dimension: 0 for x, 1 for y, 2 for z.

            Example: positions[10, 2] is z position for the 10th time index.

        velocities: numpy.ndarray
            2D array of velocities, same format as positions.

        energies: list of float
            The energy values.

        angular_momenta: numpy.ndarray
            2D array of angular momenta, same format as positions.
    """

    subdir = "tmp"
    create_dir(subdir)
    path_to_data = os.path.join(subdir, "data.bin")

    parameters = [
        (
            f'build/main {path_to_data}'
            f' --tmax={tmax}'
            f' --nt={nt}'
            f' --eccentricity={eccentricity}'
            f' --ode_approximator={ode_approximator}'
        )
    ]

    child = subprocess.Popen(parameters,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT,
                             shell=True)

    message = child.communicate()[0].decode('utf-8')
    success = child.returncode == 0

    if not success:
        print(message)
        return None

    (times, positions, velocities, energies, angular_momenta) = \
      read_solution_from_file(path_to_data)

    os.remove(path_to_data)
    os.rmdir(subdir)

    return (times, positions, velocities, energies, angular_momenta)
