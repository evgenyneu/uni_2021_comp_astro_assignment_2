module VectorsTest
use Types, only: dp
use AssertsTest, only: assert_true, assert_equal, assert_approx
use Vectors, only: cross_product
implicit none
private
public vectors_test_all

contains

subroutine cross_product_test(failures)
    integer, intent(inout) :: failures
    real(dp) :: a(3) = [1.1_dp, 0.9_dp, 12.64_dp]
    real(dp) :: b(3) = [-0.9_dp, -0.1_dp, 2.0_dp]
    real(dp) :: result(3)

    call cross_product(a=a, b=b, result=result)

    call assert_approx(result(1), 3.064_dp, 1e-15_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(result(2), -13.576_dp, 1e-15_dp, &
                       __FILE__, __LINE__, failures)

    call assert_approx(result(3), 0.7_dp, 1e-15_dp, &
                       __FILE__, __LINE__, failures)
end

subroutine vectors_test_all(failures)
    integer, intent(inout) :: failures

    call cross_product_test(failures)
end

end module VectorsTest
