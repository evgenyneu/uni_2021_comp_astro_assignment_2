module StringTest
use Types, only: dp

use AssertsTest, only: assert_approx, assert_true, &
                        assert_equal

use String, only: string_starts_with, string_contains, &
                  string_is_empty, string_to_number, str, split_string, &
                  index_from, join_strings

implicit none
private
public string_test_all

contains

! string_starts_with
! ----------------

subroutine string_starts_with_test(failures)
    integer, intent(inout) :: failures
    logical :: result

    result = string_starts_with("hello world", "hello")

    call assert_true(result, __FILE__, __LINE__, failures)
end

subroutine string_starts_with_failure_test(failures)
    integer, intent(inout) :: failures
    logical :: result

    result = string_starts_with("hello world", "possum")

    call assert_true(.not. result, __FILE__, __LINE__, failures)
end

subroutine string_starts_with_empty_strings_test(failures)
    integer, intent(inout) :: failures
    logical :: result

    result = string_starts_with("", "")

    call assert_true(result, __FILE__, __LINE__, failures)
end


! string_contains
! ----------------

subroutine string_contains_test(failures)
    integer, intent(inout) :: failures
    logical :: result

    result = string_contains("world hello world", "hello")

    call assert_true(result, __FILE__, __LINE__, failures)
end

subroutine string_contains_failure_test(failures)
    integer, intent(inout) :: failures
    logical :: result

    result = string_contains("world hello world", "nothing")

    call assert_true(.not. result, __FILE__, __LINE__, failures)
end


! string_is_empty
! ----------------

subroutine string_is_empty_test__empty(failures)
    integer, intent(inout) :: failures
    logical :: result

    result = string_is_empty("   ")

    call assert_true(result, __FILE__, __LINE__, failures)
end

subroutine string_is_empty_test__not_empty(failures)
    integer, intent(inout) :: failures
    logical :: result

    result = string_is_empty("possum")

    call assert_true(.not. result, __FILE__, __LINE__, failures)
end

! string_to_number real(dp)
! -----------------

subroutine string_to_number_test__real_dp_success(failures)
    integer, intent(inout) :: failures
    logical :: success
    real(dp) :: result

    call string_to_number("-12.42", result, success)

    call assert_true(success, __FILE__, __LINE__, failures)
    call assert_approx(result, -12.42_dp, 1e-5_dp, __FILE__, __LINE__, failures)
end

subroutine string_to_number_test__real_dp_failure(failures)
    integer, intent(inout) :: failures
    logical :: success
    real(dp) :: result

    call string_to_number("not a number", result, success)

    call assert_true(.not. success, __FILE__, __LINE__, failures)
end

! string_to_number integer(dp)
! -----------------

subroutine string_to_number_test__integer_success(failures)
    integer, intent(inout) :: failures
    logical :: success
    integer :: result

    call string_to_number("-12", result, success)

    call assert_true(success, __FILE__, __LINE__, failures)
    call assert_equal(result, -12, __FILE__, __LINE__, failures)
end

subroutine string_to_number_test__integer_failure(failures)
    integer, intent(inout) :: failures
    logical :: success
    integer :: result

    call string_to_number("not a number", result, success)

    call assert_true(.not. success, __FILE__, __LINE__, failures)
end

subroutine str_test__integer(failures)
    integer, intent(inout) :: failures

    call assert_equal(str(123), "123", __FILE__, __LINE__, failures)
    call assert_equal(str(-3), "-3", __FILE__, __LINE__, failures)
end

subroutine str_test__real_dp(failures)
    integer, intent(inout) :: failures
    character(len=100) :: result

    call assert_equal(str(0._dp), "0", __FILE__, __LINE__, failures)
    call assert_equal(str(100._dp), "100", __FILE__, __LINE__, failures)

    call assert_equal(str(123.456_dp, 0), "123.", __FILE__, __LINE__, failures)
    call assert_equal(str(123.456_dp, 1), "123.5", __FILE__, __LINE__, failures)

    result = str(0.000000012345_dp, 2, .true.)
    call assert_true(result == "1.23-08" .or. result == "1.23E-08" ,  __FILE__, &
                     __LINE__, failures)

    call assert_equal(str(2._dp), "2", __FILE__, __LINE__, failures)
    call assert_equal(str(12.1_dp), "12.1", __FILE__, __LINE__, failures)
    call assert_equal(str(12.12_dp), "12.12", __FILE__, __LINE__, failures)
    call assert_equal(str(12.123_dp), "12.123", __FILE__, __LINE__, failures)

    call assert_equal(str(1.23_dp), "1.23", __FILE__, __LINE__, failures)
    call assert_equal(str(0.123_dp), "0.123", __FILE__, __LINE__, failures)

    result = str(0.0123_dp)
    call assert_true(result == "1.23-02" .or. result == "1.23E-02", __FILE__, __LINE__, failures)

    result = str(0.00123_dp)
    call assert_true(result == "1.23-03" .or. result == "1.23E-03", __FILE__, __LINE__, failures)
end


! split_string
! -----------------

subroutine split_string_test(failures)
    integer, intent(inout) :: failures
    character(len=120), allocatable :: result(:)
    allocate(result(100))

    call split_string(trim("hello world  "), " ", result)

    call assert_equal(size(result), 2, __FILE__, __LINE__, failures)
    call assert_equal(result(1), "hello", __FILE__, __LINE__, failures)
    call assert_equal(result(2), "world", __FILE__, __LINE__, failures)
end

subroutine split_string_test___longer_searator(failures)
    integer, intent(inout) :: failures
    character(len=120), allocatable :: result(:)
    allocate(result(100))

    call split_string("This$%is$%$%string$%c$%d", "$%", result)

    call assert_equal(size(result), 6, __FILE__, __LINE__, failures)
    call assert_equal(result(1), "This", __FILE__, __LINE__, failures)
    call assert_equal(result(2), "is", __FILE__, __LINE__, failures)
    call assert_equal(result(3), "", __FILE__, __LINE__, failures)
    call assert_equal(result(4), "string", __FILE__, __LINE__, failures)
    call assert_equal(result(5), "c", __FILE__, __LINE__, failures)
    call assert_equal(result(6), "d", __FILE__, __LINE__, failures)
end

subroutine split_string_test___comma(failures)
    integer, intent(inout) :: failures
    character(len=120), allocatable :: result(:)
    allocate(result(100))

    call split_string("a,b,,c,,de,f", ",", result)

    call assert_equal(size(result), 7, __FILE__, __LINE__, failures)
    call assert_equal(result(1), "a", __FILE__, __LINE__, failures)
    call assert_equal(result(2), "b", __FILE__, __LINE__, failures)
    call assert_equal(result(3), "", __FILE__, __LINE__, failures)
    call assert_equal(result(4), "c", __FILE__, __LINE__, failures)
    call assert_equal(result(5), "", __FILE__, __LINE__, failures)
    call assert_equal(result(6), "de", __FILE__, __LINE__, failures)
    call assert_equal(result(7), "f", __FILE__, __LINE__, failures)
end

subroutine split_string_test___empty_string(failures)
    integer, intent(inout) :: failures
    character(len=*), parameter :: str = ""
    character(len=120), allocatable :: result(:)

    call split_string(str, " ", result)

    call assert_equal(size(result), 1, __FILE__, __LINE__, failures)
    call assert_equal(result(1), "", __FILE__, __LINE__, failures)
end


subroutine split_string_test___empty_string_two(failures)
    integer, intent(inout) :: failures
    character(len=2) :: str
    character(len=2), allocatable :: result(:)
    allocate(result(3))
    str = ""

    call split_string(str, " ", result)

    call assert_equal(size(result), 3, __FILE__, __LINE__, failures)
    call assert_equal(result(1), "", __FILE__, __LINE__, failures)
    call assert_equal(result(2), "", __FILE__, __LINE__, failures)
    call assert_equal(result(3), "", __FILE__, __LINE__, failures)
end


! index_from
! -----------------

subroutine index_from_test(failures)
    integer, intent(inout) :: failures
    integer :: result

    result = index_from("iThis is a string", "i", 1)
    call assert_equal(result, 1, __FILE__, __LINE__, failures)

    result = index_from("iThis is a string", "i", 2)
    call assert_equal(result, 4, __FILE__, __LINE__, failures)

    result = index_from("iThis is a string", "i", 3)
    call assert_equal(result, 4, __FILE__, __LINE__, failures)

    result = index_from("iThis is a string", "i", 4)
    call assert_equal(result, 4, __FILE__, __LINE__, failures)

    result = index_from("iThis is a string", "i", 5)
    call assert_equal(result, 7, __FILE__, __LINE__, failures)

    result = index_from("iThis is a string", "i", 10)
    call assert_equal(result, 15, __FILE__, __LINE__, failures)

    result = index_from("iThis is a string", "i", 17)
    call assert_equal(result, 0, __FILE__, __LINE__, failures)

    result = index_from("This is a string", "z", 1)
    call assert_equal(result, 0, __FILE__, __LINE__, failures)
end


! join_strings_test
! -----------------

subroutine join_strings_test(failures)
    integer, intent(inout) :: failures
    character(len=100) :: result

    call join_strings(['one', 'two'], ", ", result)

    call assert_equal(result, "one, two", __FILE__, __LINE__, failures)
end

subroutine join_strings_test__empty(failures)
    integer, intent(inout) :: failures
    character(len=100) :: result
    character(len=100) :: strings(0)

    call join_strings(strings, ", ", result)

    call assert_equal(result, "", __FILE__, __LINE__, failures)
end


subroutine string_test_all(failures)
    integer, intent(inout) :: failures

    call string_starts_with_failure_test(failures)
    call string_starts_with_test(failures)
    call string_starts_with_empty_strings_test(failures)

    call string_is_empty_test__empty(failures)
    call string_is_empty_test__not_empty(failures)

    call string_to_number_test__real_dp_success(failures)
    call string_to_number_test__real_dp_failure(failures)

    call string_to_number_test__integer_success(failures)
    call string_to_number_test__integer_failure(failures)

    call str_test__integer(failures)
    call str_test__real_dp(failures)

    call string_contains_test(failures)
    call string_contains_failure_test(failures)

    call split_string_test(failures)
    call split_string_test___comma(failures)
    call split_string_test___longer_searator(failures)
    call split_string_test___empty_string(failures)
    call split_string_test___empty_string_two(failures)

    call index_from_test(failures)

    call join_strings_test(failures)
    call join_strings_test__empty(failures)
end

end module StringTest
