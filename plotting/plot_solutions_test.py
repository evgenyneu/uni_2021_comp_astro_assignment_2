import numpy as np
import matplotlib.pyplot as plt
from plot_solution import plot, plot_trajectories
import os
import shutil



def test_plot_solution():
    fig, axes = plt.subplots(3, 2, figsize=(9,9))

    plot(ax=axes[0,0],
         eccentricity=0,
         nt=6300,
         dt=0.1,
         ode_approximator='leapfrog',
         color='red')


def test_plot_trajectories():
    plot_dir = "test_plot"
    plot_file_name = "test_trajectories.pdf"
    plot_file_path = os.path.join(plot_dir, plot_file_name)

    if os.path.exists(plot_file_path):
        os.remove(plot_file_path)

    colors = {
        "leapfrog": "orangered",
        "runge_kutta": "deeppink"
    }

    plot_trajectories(
        plot_dir=plot_dir,
        file_name=plot_file_name,
        colors=colors
    )

    assert os.path.exists(plot_file_path)
    os.remove(plot_file_path)
    shutil.rmtree(plot_dir)



